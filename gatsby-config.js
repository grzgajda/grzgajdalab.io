/* eslint-disable @typescript-eslint/no-var-requires */
const {
  compression,
  typescript,
  reactHelmet,
  sourceFilesystem,
  transformerSharp,
  sharp,
  manifest,
  offline,
  markdownRemark,
  fontLoader,
  sitemap,
  styledComponents,
  googleTagManager,
  robotsTxt,
} = require('./config/plugins')
/* eslint-enable */

module.exports = {
  plugins: [
    // styles
    fontLoader('Montserrat:400,700'),
    styledComponents(),
    // typescript
    typescript(),
    // seo
    reactHelmet(),
    manifest('devMint', 'rgb(46, 170, 170)', 'logo-avatar.png'),
    sitemap(),
    robotsTxt(),
    googleTagManager('UA-131405872-1'),
    // images
    sourceFilesystem('images', `${__dirname}/src/images`),
    transformerSharp(),
    sharp(),
    // markdown
    sourceFilesystem('posts', `${__dirname}/posts`),
    markdownRemark(),
    // offline
    compression(),
    offline(),
  ],
  siteMetadata: {
    headerLinks: [
      { label: 'Index', url: '/' },
      { label: 'Tags', url: '/tags' },
      { label: 'About', url: '/devmint-a-mind-of-creativity' },
    ],
    metaDescription:
      'Prosty blog o tematyce programistycznej, od programisty dla programistów. Omawiane są pojęcia z tematyki architektury aplikacji, konstrukcji językowych lub róne use cases dla wzorców projektowych.',
    metaTitle: 'devMint - a mind of creativity',
    siteUrl: 'https://devmint.pl',
    title: 'devMint',
  },
}
