# Building React's application
FROM node:12.8.1

WORKDIR /app

# Install dependencies
COPY package.json yarn.lock /app/
RUN yarn install

COPY . .
RUN yarn build

# Exposing HTML, CSS, JS files to world
FROM nginx:latest

RUN rm /etc/nginx/conf.d/default.conf

COPY --from=0 /app/public /usr/share/nginx/html
COPY --from=0 /app/docker/nginx/conf.d /etc/nginx/conf.d/default.conf