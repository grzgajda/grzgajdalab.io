import { storiesOf } from '@storybook/react'
import { text } from '@storybook/addon-knobs'
import React from 'react'
import {
  Paragraph,
  Strong,
  H1,
  H2,
  H3,
  UnorderedList,
  OrderedList,
  ListItem,
} from '../components/Typography'
import CenterBox from './addons/CenterBox'

const LOREM_IPSUM_1 = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque molestie suscipit diam nec pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent quis metus imperdiet, elementum nulla eget, laoreet felis. Pellentesque pulvinar urna vulputate pharetra placerat. In tempus lectus sed massa elementum, at tempor purus aliquam. Donec id sapien sodales, vehicula libero sit amet, ullamcorper augue. Curabitur mollis massa imperdiet tincidunt fermentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.`
const LOREM_IPSUM_2 = `Integer gravida rutrum nisi id maximus. Mauris finibus iaculis blandit. Sed vel blandit libero. Pellentesque enim magna, tincidunt a hendrerit eget, feugiat vel dui. Phasellus placerat vel elit et tincidunt. Aenean condimentum nisi id fermentum elementum. Aliquam quis mollis ante. Proin condimentum cursus velit ac sollicitudin. Vivamus nibh nulla, volutpat sit amet varius in, fringilla quis orci. Nunc at eros a tortor semper pharetra. Suspendisse ipsum neque, ultricies eu risus in, auctor sagittis nulla. Vivamus tincidunt erat non orci cursus interdum. Cras non suscipit nunc.`

storiesOf('Typography | Paragraph', module)
  .addDecorator(storyFn => (
    <CenterBox asColumn={true} style={{ margin: '0 auto', width: '600px' }}>
      {storyFn()}
    </CenterBox>
  ))
  .add('single', () => <Paragraph>{text('content', LOREM_IPSUM_1)}</Paragraph>)
  .add('multi', () => (
    <React.Fragment>
      <Paragraph>{text('content 1', LOREM_IPSUM_1)}</Paragraph>
      <Paragraph>{text('content 2', LOREM_IPSUM_2)}</Paragraph>
    </React.Fragment>
  ))
  .add('for article', () => (
    <React.Fragment>
      <Paragraph article={true}>{text('content 1', LOREM_IPSUM_1)}</Paragraph>
      <Paragraph article={true}>{text('content 2', LOREM_IPSUM_2)}</Paragraph>
    </React.Fragment>
  ))

storiesOf('Typography | Strong', module)
  .addDecorator(storyFn => (
    <CenterBox asColumn={true} style={{ margin: '0 auto', width: '600px' }}>
      {storyFn()}
    </CenterBox>
  ))
  .add('single', () => <Strong>{text('content', LOREM_IPSUM_1)}</Strong>)
  .add('+ paragraph', () => (
    <React.Fragment>
      <Strong>{text('content 1', LOREM_IPSUM_1)}</Strong>
      <Paragraph>{text('content 2', LOREM_IPSUM_2)}</Paragraph>
    </React.Fragment>
  ))

storiesOf('Typography | Header', module)
  .addDecorator(storyFn => (
    <CenterBox asColumn={true} style={{ margin: '0 auto', width: '600px' }}>
      {storyFn()}
    </CenterBox>
  ))
  .add('h1, h2, h3', () => (
    <React.Fragment>
      <H1>H1: {text('h1 text', LOREM_IPSUM_1.substr(0, 40))}</H1>
      <H2>H2: {text('h2 text', LOREM_IPSUM_1.substr(0, 40))}</H2>
      <H3>H3: {text('h3 text', LOREM_IPSUM_1.substr(0, 40))}</H3>
    </React.Fragment>
  ))

storiesOf('Typography | List', module)
  .addDecorator(storyFn => (
    <CenterBox asColumn={true} style={{ margin: '0 auto', width: '600px' }}>
      {storyFn()}
    </CenterBox>
  ))
  .add('unordered list', () => (
    <UnorderedList>
      <ListItem>Lorem ipsum</ListItem>
      <ListItem>Dolor sit amet</ListItem>
    </UnorderedList>
  ))
  .add('ordered list', () => (
    <OrderedList>
      <ListItem>Lorem ipsum</ListItem>
      <ListItem>Dolor sit amet</ListItem>
    </OrderedList>
  ))
