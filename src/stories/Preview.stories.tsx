import { storiesOf } from '@storybook/react'
import React from 'react'
import { text } from '@storybook/addon-knobs'
import Preview from '../components/Preview/Preview'
import exampleCover from '../../posts/2019/kompozycja-funkcji/cover.png'
import CenterBox from './addons/CenterBox'
import { createFixedImage } from './addons/createImage'

const EXCERPT =
  'Jednym z paradygmatów programowania obiektowego jest dziedziczenie, które polega na rozszerzeniu klasy bazowej o nowe metody lub nadpisanie już...'

storiesOf('Template | Preview', module)
  .addDecorator(storyFn => <CenterBox>{storyFn()}</CenterBox>)
  .add('default', () => (
    <Preview
      excerpt={text('excerpt', EXCERPT)}
      path={text('path', '/')}
      title={text('title', 'Kompozycja funkcji')}
      desktop={createFixedImage(exampleCover)}
    />
  ))
  .add('without cover', () => (
    <Preview
      excerpt={text('excerpt', EXCERPT)}
      path={text('path', '/')}
      title={text('title', 'Kompozycja funkcji')}
    />
  ))
