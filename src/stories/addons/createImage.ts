import { GatsbyImageProps } from 'gatsby-image'

export const createFixedImage = (
  url: string,
  width = 335,
  height = 175
): GatsbyImageProps => ({
  fixed: {
    height,
    src: url,
    srcSet: url,
    width,
  },
})

export const createFluidImage = (
  url: string,
  width = 335,
  height = 175
): GatsbyImageProps => ({
  fluid: {
    aspectRatio: width / height,
    sizes: '',
    src: url,
    srcSet: url,
  },
})
