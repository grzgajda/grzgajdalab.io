import is from 'typescript-styled-is'
import styled from '../../theme'

const CenterBox = styled.div<ICenterBoxProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  height: 100vh;
  margin: 50px auto;

  ${is('asColumn')`flex-direction: column;`};
  ${is('asRow')`flex-direction: row;`};
`

export default CenterBox
interface ICenterBoxProps {
  asColumn?: boolean
  asRow?: boolean
}
