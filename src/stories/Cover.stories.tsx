import { storiesOf } from '@storybook/react'
import React from 'react'
import { text } from '@storybook/addon-knobs'
import Cover from '../components/Cover'
import exampleCover1 from '../../posts/2019/kompozycja-funkcji/cover.png'
import CenterBox from './addons/CenterBox'
import { createFixedImage } from './addons/createImage'

storiesOf('Template | Cover', module)
  .addDecorator(storyFn => <CenterBox asColumn={true}>{storyFn()}</CenterBox>)
  .add('default', () => (
    <Cover
      cover={createFixedImage(exampleCover1)}
      title={text('title', 'Lorem ipsum')}
    />
  ))
