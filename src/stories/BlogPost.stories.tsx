import { storiesOf } from '@storybook/react'
import React from 'react'
import { text } from '@storybook/addon-knobs'
import BlogPost from '../components/BlogPost/BlogPost'
import { Paragraph } from '../components/Typography'
import exampleCover1 from '../../posts/2019/kompozycja-funkcji/cover.png'
import CenterBox from './addons/CenterBox'
import { createFixedImage } from './addons/createImage'

const CONTENT =
  'Jednym z paradygmatów programowania obiektowego jest dziedziczenie, które polega na rozszerzeniu klasy bazowej o nowe metody lub nadpisanie już...'
const FULL_CONTENT = `
  Jednym z paradygmatów programowania obiektowego jest dziedziczenie,
  które polega na rozszerzeniu klasy bazowej o nowe metody lub nadpisanie
  już istniejących. Inną metodą rozszerzania istniejących klas jest ich
  kompozycja.
`.trim()

storiesOf('Template | Blog Post', module)
  .addDecorator(storyFn => <CenterBox asColumn={true}>{storyFn()}</CenterBox>)
  .add('default', () => (
    <BlogPost
      cover={createFixedImage(exampleCover1, 700, 335)}
      title={text('title', 'Lorem ipsum dolor sit amet')}
      introduction={text('introduction', FULL_CONTENT)}
    >
      <Paragraph>{text('content', CONTENT)}</Paragraph>
    </BlogPost>
  ))
  .add('without introduction', () => (
    <BlogPost
      cover={createFixedImage(exampleCover1, 700, 335)}
      title={text('title', 'Lorem ipsum dolor sit amet')}
    >
      <Paragraph>{text('content', CONTENT)}</Paragraph>
    </BlogPost>
  ))
  .add('without cover', () => (
    <BlogPost
      title={text('title', 'Lorem ipsum dolor sit amet')}
      introduction={text('introduction', FULL_CONTENT)}
    >
      <Paragraph>{text('content', CONTENT)}</Paragraph>
    </BlogPost>
  ))
  .add('without cover and introduction', () => (
    <BlogPost title={text('title', 'Lorem ipsum dolor sit amet')}>
      <Paragraph>{text('content', CONTENT)}</Paragraph>
    </BlogPost>
  ))
