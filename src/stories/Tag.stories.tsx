import { storiesOf } from '@storybook/react'
import React from 'react'
import { boolean, text, number } from '@storybook/addon-knobs'
import Tag from '../components/Tag/Tag'
import TagList from '../components/TagList/TagList'
import logoJs from '../../posts/__tags/javascript.png'
import logoTs from '../../posts/__tags/typescript.png'
import CenterBox from './addons/CenterBox'
import { createFixedImage } from './addons/createImage'

storiesOf('Template | Tag', module)
  .addDecorator(storyFn => <CenterBox asRow={true}>{storyFn()}</CenterBox>)
  .add('single', () => (
    <Tag
      path={text('path', '/javascript')}
      title={text('title', 'Javascript')}
      desktop={createFixedImage(logoJs, 100, 100)}
      isSelected={boolean('is selected', false)}
    />
  ))
  .add('selected', () => (
    <Tag
      path={text('path', '/javascript')}
      title={text('title', 'Javascript')}
      desktop={createFixedImage(logoJs, 100, 100)}
      isSelected={boolean('is selected', true)}
    />
  ))
  .add('small (width < 600px)', () => (
    <Tag
      path={text('path', '/javascript')}
      title={text('title', 'Javascript')}
      desktop={createFixedImage(logoJs, 50, 50)}
      isSelected={boolean('is selected', true)}
    />
  ))
  .add('list', () => (
    <React.Fragment>
      <Tag
        path={text('path js', '/javascript', 'Javascript')}
        title={text('title js', 'Javascript', 'Javascript')}
        desktop={createFixedImage(logoJs, 100, 100)}
        isSelected={boolean('is selected js', true, 'Javascript')}
      />
      <Tag
        path={text('path ts', '/typescript', 'TypeScript')}
        title={text('title ts', 'TypeScript', 'TypeScript')}
        desktop={createFixedImage(logoTs, 100, 100)}
        isSelected={boolean('is selected ts', true, 'TypeScript')}
      />
    </React.Fragment>
  ))
  .add('tag list', () => (
    <TagList>
      {Array.from(new Array(number('num of elements', 50))).map(() => (
        <Tag
          path={text('path ts', '/typescript', 'TypeScript')}
          title={text('title ts', 'TypeScript', 'TypeScript')}
          desktop={createFixedImage(logoTs, 100, 100)}
          isSelected={boolean('is selected ts', true, 'TypeScript')}
        />
      ))}
    </TagList>
  ))
