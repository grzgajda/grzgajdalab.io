import { storiesOf } from '@storybook/react'
import { boolean, text } from '@storybook/addon-knobs'
import React from 'react'
import Logo from '../components/Logo'
import logo from '../images/logo.svg'

storiesOf('Template | Logo', module)
  .add('default', () => (
    <Logo
      siteName={text('site name', 'devMint')}
      iconSrc={logo}
      withBorder={boolean('with border', true)}
    />
  ))
  .add('without border', () => (
    <Logo
      siteName={text('site name', 'devMint')}
      iconSrc={logo}
      withBorder={boolean('with border', false)}
    />
  ))
