import styled from '../../theme'

const TagList = styled.section`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;

  width: 100%;
  padding: 0 15px;

  @media (min-width: 768px) {
    max-width: var(--container-width);
    margin: 0 auto var(--container-margin) auto;
  }
`

export default TagList
