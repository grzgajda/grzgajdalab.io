import React from 'react'
import { Wrapper, Border, Icon, Title } from './styles'

function Logo({ siteName, iconSrc, withBorder = false }: ILogoProps) {
  return (
    <Wrapper>
      <Title>{siteName}</Title>
      <Icon src={iconSrc} alt={siteName} />
      {withBorder && <Border />}
    </Wrapper>
  )
}

export default Logo
export interface ILogoProps {
  siteName: string
  iconSrc: string
  withBorder?: boolean
}
