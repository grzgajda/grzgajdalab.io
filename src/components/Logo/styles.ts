import styled from '../../theme'

export const Wrapper = styled.div`
  max-width: calc(var(--container-width) - 40px);
  width: calc(100% - 40px);
  margin: var(--container-small-margin) auto;
  @media (min-width: 768px) {
    margin: var(--container-margin) auto;
  }
`

export const Title = styled.h1`
  display: inline;
  color: var(--header-color);

  font-size: 48px;
  @media (min-width: 375px) {
    font-size: 64px;
  }
`

export const Icon = styled.img`
  width: 26px;
`

export const Border = styled.div`
  width: 100%;
  height: 1px;
  background-color: var(--colors-border);
  margin-top: var(--container-small-margin);

  @media (min-width: 768px) {
    margin-top: var(--container-margin);
  }
`
