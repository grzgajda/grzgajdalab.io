import React from 'react'
import { Link } from 'gatsby'
import { GatsbyImageProps } from 'gatsby-image'
import Cover from '../Cover'
import { Wrapper, DesktopTag, MobileTag } from './styles'

const Tag = ({
  path,
  title,
  desktop,
  mobile,
  isSelected,
}: ITagQuery & ITagProps) => (
  <Wrapper selected={isSelected}>
    <Link to={path} title={title}>
      {desktop && (
        <DesktopTag>
          <Cover cover={desktop} title={title} />
        </DesktopTag>
      )}

      {mobile && (
        <MobileTag>
          <Cover cover={mobile} title={title} />
        </MobileTag>
      )}
    </Link>
  </Wrapper>
)

export default Tag
interface ITagQuery {
  path: string
  title: string
  desktop?: GatsbyImageProps
  mobile?: GatsbyImageProps
}
interface ITagProps {
  isSelected?: boolean
}
