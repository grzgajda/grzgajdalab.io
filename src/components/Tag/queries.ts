import { graphql } from 'gatsby'

export const TagQuery = graphql`
  fragment Tag on MarkdownRemark {
    frontmatter {
      title
      path
      shortTitle
      desktop: cover {
        ...TagCover
      }
      mobile: cover {
        ...SmallTagCover
      }
    }
  }
`
