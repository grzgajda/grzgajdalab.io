import is from 'typescript-styled-is'
import styled, { css } from '../../theme'

export const Wrapper = styled.div`
  width: var(--tag-size);
  height: var(--tag-size);

  & + & {
    margin-left: var(--container-small-margin);
    @media (min-width: 600px) {
      margin-left: var(--container-margin);
    }
  }

  &:last-child {
    padding-right: calc(15px + var(--container-margin));
    @media (min-width: 600px) {
      padding-right: calc(3 * var(--container-margin));
    }
  }

  opacity: 0.6;
  &:hover {
    opacity: 1;
  }

  ${is('selected')`
    opacity: 1;
  `}
`

const hide = css`
  display: none;
  opacity: 0;
  visibility: hidden;
`
export const DesktopTag = styled.div`
  @media (max-width: 600px) {
    ${hide};
  }
`
export const MobileTag = styled.div`
  @media (min-width: 600px) {
    ${hide};
  }
`
