import { graphql } from 'gatsby'

export const PostCoverQuery = graphql`
  fragment PostCover on File {
    childImageSharp {
      fixed(width: 335, height: 175) {
        ...GatsbyImageSharpFixed_withWebp
      }
    }
  }
`
export const FluidPostCoverQuery = graphql`
  fragment FluidPostCover on File {
    childImageSharp {
      fluid(maxWidth: 500) {
        ...GatsbyImageSharpFluid_withWebp
      }
    }
  }
`
export const FluidBigPostCoverQuery = graphql`
  fragment FluidBigPostCover on File {
    childImageSharp {
      fluid(maxWidth: 700) {
        ...GatsbyImageSharpFluid_withWebp
      }
    }
  }
`
export const TagCoverQuery = graphql`
  fragment TagCover on File {
    childImageSharp {
      fixed(width: 100, height: 100) {
        ...GatsbyImageSharpFixed_withWebp
      }
    }
  }
`
export const SmallTagCoverQuery = graphql`
  fragment SmallTagCover on File {
    childImageSharp {
      fixed(width: 50, height: 50) {
        ...GatsbyImageSharpFixed_withWebp
      }
    }
  }
`

export const AdminPreviewCoverQuery = graphql`
  fragment AdminPreviewCover on File {
    childImageSharp {
      fluid(maxWidth: 200) {
        ...GatsbyImageSharpFluid_withWebp
      }
    }
  }
`
