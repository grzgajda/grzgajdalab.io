import React from 'react'
import { GatsbyImageProps } from 'gatsby-image'
import { StyledImg } from './styles'

const Cover = ({ cover, title = '', withoutShadow }: CoverProps) => (
  <StyledImg
    alt={title}
    title={title}
    withoutShadow={withoutShadow}
    {...cover}
  />
)

export default Cover
type CoverProps = ICoverQuery
interface ICoverQuery {
  cover: GatsbyImageProps
  title?: string
  withoutShadow?: boolean
}
