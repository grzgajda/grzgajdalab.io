import Image from 'gatsby-image'
import { isNot } from 'typescript-styled-is'
import styled from '../../theme'

export const StyledImg = styled(Image)`
  border-radius: var(--image-radius);
  ${isNot('withoutShadow')`
    box-shadow: var(--image-shadow);
  `}
`
