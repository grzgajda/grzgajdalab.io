import styled from '../../theme'
import Paragraph from './Paragraph'

export const Blockquote = styled(Paragraph.withComponent('blockquote'))`
  position: relative;
  margin: 0;
  text-align: center;

  @media (min-width: 600px) {
    margin: 0 40px;
  }

  & > ${Paragraph} {
    opacity: 0.85;
    font-size: 1.1em;

    &:before,
    &:after {
      display: inline;
      color: var(--colors-light);
      font-size: 2em;
      line-height: 0;
    }
    &:before {
      content: '„';
      margin-right: 4px;
    }
    &:after {
      content: '”';
      margin-left: 4px;
    }
  }
`
