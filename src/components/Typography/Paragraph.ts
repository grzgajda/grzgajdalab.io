import is from 'typescript-styled-is'
import styled from '../../theme'

const Paragraph = styled.p<IIsArticle>`
  font-family: var(--text-font);
  font-size: 15px;
  color: var(--text-color);
  line-height: 1.8em;
  display: block;

  ${is('article')`
    margin: 25px auto;
  `}

  & > code {
    color: var(--colors-light);
    font-family: Consolas, Monaco, 'Andale Mono', monospace;
  }
`

export default Paragraph
interface IIsArticle {
  article?: boolean
}
