import styled, { css } from '../../theme'
import Paragraph from './Paragraph'

const decreasePaddingToParagraph = css`
  padding-bottom: 0;
  margin-bottom: 0;
  & + ${Paragraph} {
    margin-top: 0;
  }
`

export const H1 = styled(Paragraph.withComponent('h1'))`
  ${decreasePaddingToParagraph}
  font-size: 1.4em;
`
export const H2 = styled(Paragraph.withComponent('h2'))`
  ${decreasePaddingToParagraph}
  font-size: 1.1em;
`
export const H3 = styled(Paragraph.withComponent('h3'))`
  ${decreasePaddingToParagraph}
  font-size: 1em;
`
