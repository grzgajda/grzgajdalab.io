import styled from '../../theme'
import Paragraph from './Paragraph'

const Strong = styled(Paragraph.withComponent('strong'))`
  font-weight: 700;
  display: inline;
`

export default Strong
