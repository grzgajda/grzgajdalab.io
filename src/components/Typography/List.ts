import styled from '../../theme'
import greenTick from '../../images/ticks/green-tick.svg'
import Paragraph from './Paragraph'

export const ListItem = styled(Paragraph.withComponent('li'))``
const List = styled.ul`
  padding: 0;
  list-style-type: none;
`
export const UnorderedList = styled(List.withComponent('ul'))`
  & > ${ListItem} {
    position: relative;
    text-indent: 24px;

    &:before {
      content: '';
      display: block;
      position: absolute;
      width: 16px;
      height: 16px;
      left: 0;
      top: 6px;

      background-size: 16px;
      background-position: center center;
      background-repeat: no-repeat;
      background-image: url(${greenTick});
    }
  }
`
export const OrderedList = styled(List.withComponent('ol'))``
