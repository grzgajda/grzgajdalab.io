import React from 'react'
import { GatsbyImageProps } from 'gatsby-image'
import Cover from '../Cover'
import {
  CoverWrapper,
  Introduction,
  Wrapper,
  Title,
  ContentWrapper,
} from './styles'

const BlogPost = ({
  cover,
  introduction,
  title,
  children,
  publishedAt,
  schema,
}: IBlogPostQuery & IBlogPostProps) => (
  <Wrapper itemScope itemType={'https://schema.org/TechArticle'}>
    {schema && schema(publishedAt, cover)}

    <header>
      <Title>{title}</Title>
      {introduction && (
        <Introduction itemProp={'headline'}>{introduction}</Introduction>
      )}

      {cover && (
        <CoverWrapper>
          <Cover cover={cover} title={title} />
        </CoverWrapper>
      )}
    </header>

    <ContentWrapper itemProp={'articleBody'}>{children}</ContentWrapper>
  </Wrapper>
)

export default BlogPost
export interface IBlogPostProps {
  children: React.ReactNode
  schema?: (
    publishedAt: string,
    cover: GatsbyImageProps | undefined
  ) => React.ReactNode
}
interface IBlogPostQuery {
  title: string
  publishedAt: string
  introduction?: string
  cover?: GatsbyImageProps
}
