import styled from '../../theme'
import { shadowed } from '../../theme/mixins'
import { Content } from '../Preview/styles'
import { Paragraph, Strong } from '../Typography'

export const Wrapper = styled(Content.withComponent('article'))`
  max-width: calc(100% - 30px);
  width: 100%;

  && {
    margin: 0 auto var(--container-margin);
    @media (min-width: 768px) {
      padding: 0 15px;
    }
  }

  @media (min-width: 768px) {
    max-width: 700px;
  }
`

export const CoverWrapper = styled.div`
  margin-top: var(--container-margin);
  margin-bottom: var(--container-margin);
`

export const Title = styled(Strong.withComponent('h1'))`
  font-size: 34px;
  @media (min-width: 768px) {
    font-size: 42px;
  }

  font-weight: 500;
  color: var(--header-color);
  margin-top: 0;
  margin-bottom: 0;
`

export const Introduction = styled(Paragraph.withComponent('p'))`
  font-size: 17px;
  @media (min-width: 768px) {
    font-size: 20px;
  }

  font-weight: 400;
  opacity: 0.65;
`

export const ContentWrapper = styled.main`
  .gatsby-resp-image-wrapper {
    ${shadowed};
  }
  .gatsby-resp-image-image,
  .gatsby-resp-image-background-image {
    border-radius: var(--image-radius);
  }
`
