import { graphql } from 'gatsby'

export const BlogPostQueries = graphql`
  fragment BlogPost on MarkdownRemark {
    content: htmlAst
    excerpt(pruneLength: 300)
    frontmatter {
      title
      path
      tags
      intro
      publishedAt: date(formatString: "DD-MM-YYYY")
      shortTitle
      cover {
        ...FluidBigPostCover
      }
    }
  }
`
