import { GatsbyImageProps } from 'gatsby-image'
import React, { Fragment } from 'react'
import * as Schema from '../../containers/Schema'
import BlogPost from './BlogPost'

export const withSchema = (
  publishedAt: string,
  cover: GatsbyImageProps | undefined
): React.ReactNode => (
  <Fragment>
    <meta itemProp={'datePublished'} content={publishedAt} />
    <Schema.Image imageUrl={cover} />
    <Schema.Person />
    <Schema.Organization />
  </Fragment>
)

export default (
  C: React.ComponentType<React.ComponentProps<typeof BlogPost>>
) => (props: React.ComponentProps<typeof BlogPost>) => (
  <C schema={withSchema} {...props} />
)
