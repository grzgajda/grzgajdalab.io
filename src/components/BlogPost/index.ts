import BlogPost from './BlogPost'
import './queries'
import withSchema from './withSchema'

export default withSchema(BlogPost)
