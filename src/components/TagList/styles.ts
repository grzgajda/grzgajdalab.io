import styled from '../../theme'

export const Wrapper = styled.section`
  overflow: hidden;
  width: 100%;
  height: calc(var(--tag-size) + 20px);
  margin: var(--container-small-margin) 0;

  @media (min-width: 600px) {
    margin: var(--container-margin) 0;
  }

  position: relative;
`

export const NestedList = styled.div`
  position: absolute;
  width: 100%;
  overflow-x: scroll;
  transition: 1s transform;
  font-size: 0;
  white-space: nowrap;
  padding-bottom: 35px;
  box-sizing: content-box;

  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row;

  & > * {
    &:first-child {
      margin-left: 15px;
      @media (min-width: 768px) {
        margin-left: calc(100vw / 2 - 335px);
      }
    }
    &:last-child {
      margin-right: 15px;
    }
  }
`
