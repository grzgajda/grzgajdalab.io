import React from 'react'
import { NestedList, Wrapper } from './styles'

const TagList = ({ children }: ITagListProps) => (
  <Wrapper>
    <NestedList>{children}</NestedList>
  </Wrapper>
)

export default TagList
interface ITagListProps {
  children: React.ReactNode
}
