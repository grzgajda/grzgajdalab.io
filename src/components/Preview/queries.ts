import { graphql } from 'gatsby'

export const PostPreviewQuery = graphql`
  fragment PostPreview on MarkdownRemark {
    frontmatter {
      title
      path
      desktop: cover {
        ...PostCover
      }
      mobile: cover {
        ...FluidPostCover
      }
      intro
    }
    excerpt
  }
`
