import React from 'react'
import { GatsbyImageProps } from 'gatsby-image'
import { Link } from 'gatsby'
import textTruncate from '../../utils/textTruncate'
import Cover from '../Cover'
import { Paragraph, Strong } from '../Typography'
import {
  Article,
  Content,
  MobileCoverWrapper,
  DesktopCoverWrapper,
} from './styles'

const Preview = ({ excerpt, title, path, desktop, mobile }: IPreviewQuery) => {
  const previewText =
    excerpt && (desktop || mobile) ? textTruncate(excerpt, 100) : excerpt

  return (
    <Article itemScope itemType={'https://schema.org/TechArticle'}>
      <Link to={path}>
        {desktop && (
          <React.Fragment>
            {desktop.fixed && (
              <meta itemProp={'image'} content={desktop.fixed.src} />
            )}
            {desktop.fluid && (
              <meta itemProp={'image'} content={desktop.fluid.src} />
            )}
            <DesktopCoverWrapper>
              <Cover cover={desktop} title={title} />
            </DesktopCoverWrapper>
          </React.Fragment>
        )}

        {mobile && (
          <MobileCoverWrapper>
            <Cover cover={mobile} title={title} />
          </MobileCoverWrapper>
        )}

        <Content full={true}>
          <Strong itemProp={'headline'}>{title}</Strong>
          <Paragraph itemProp={'alternativeHeadline'}>{previewText}</Paragraph>
        </Content>
      </Link>
    </Article>
  )
}

export default Preview
interface IPreviewQuery {
  title: string
  path: string
  desktop?: GatsbyImageProps
  mobile?: GatsbyImageProps
  excerpt: string
}
