import is from 'typescript-styled-is'
import styled, { css } from '../../theme'
import { Paragraph, Strong } from '../Typography'

const centerFlex = css`
  display: flex;
  align-items: center;
  justify-content: center;

  flex-direction: column;
  @media (min-width: 600px) {
    flex-direction: row;
  }
`

export const Article = styled.article`
  max-width: var(--container-width);
  width: 100%;
  margin: 0 auto;
  & + & {
    margin-top: var(--container-margin);
  }

  &,
  & > a {
    max-width: var(--container-width);
    ${centerFlex};
  }

  & > a:hover {
    cursor: pointer;

    ${Strong} {
      color: var(--colors-light);
    }
  }

  :last-child {
    margin-bottom: var(--container-margin);
  }
`

export const Content = styled.div<{ full?: boolean }>`
  display: flex;
  flex-direction: column;

  margin-top: 20px;
  @media (min-width: 600px) {
    max-width: calc(100% - 355px);
    ${is('full')`
      max-width: 100%;
    `}
    margin-top: 0;
    margin-left: 20px;
  }

  & > ${Paragraph}, & > ${Strong} {
    max-width: 100%;
    width: 100%;
    padding-left: 0;
    margin: 0;
  }

  & > ${Strong} + ${Paragraph} {
    margin-top: 20px;
    @media (min-width: 768px) {
      margin-top: 10px;
    }
  }

  & > ${Strong} {
    color: var(--header-color);
    font-size: 1.1em;
  }
`

export const CoverWrapper = styled.div`
  max-width: 700px;
  width: 100%;

  @media (min-width: 600px) {
    width: 335px;
    height: 180px;
  }
`

const hide = css`
  display: none;
  opacity: 0;
  visibility: hidden;
`
export const DesktopCoverWrapper = styled(CoverWrapper)`
  @media (max-width: 600px) {
    ${hide};
  }
`
export const MobileCoverWrapper = styled(CoverWrapper)`
  @media (min-width: 600px) {
    ${hide};
  }
`
