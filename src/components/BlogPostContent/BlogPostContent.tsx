import React from 'react'
import rehype from 'rehype-react'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'
import {
  Blockquote,
  Paragraph,
  Strong,
  H1,
  H2,
  H3,
  UnorderedList,
  OrderedList,
  ListItem,
} from '../../components/Typography'
import { Frame, Syntax } from './styles'

const { Compiler } = new rehype({
  components: {
    blockquote: Blockquote,
    h1: H1,
    h2: H2,
    h3: H3,
    iframe: Frame,
    li: ListItem,
    ol: OrderedList,
    p: Paragraph,
    pre: (args: { children: React.ReactElement[] }) => {
      const { children, className } = args.children[0].props as {
        className: string
        children: React.ReactElement
      }

      return (
        <Syntax style={darcula} language={className.replace('language-', '')}>
          {children}
        </Syntax>
      )
    },
    strong: Strong,
    ul: UnorderedList,
  },
  createElement: React.createElement,
})

const BlogPostContent = ({ children }: IBlogPostContentProps) =>
  Compiler(children)

export default BlogPostContent
interface IBlogPostContentProps {
  children: React.ReactNode
}
