import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import styled from '../../theme'
import { shadowed } from '../../theme/mixins'

export const Syntax = styled(SyntaxHighlighter)`
  border-radius: var(--image-radius);
`

export const Frame = styled.iframe`
  ${shadowed};
`
