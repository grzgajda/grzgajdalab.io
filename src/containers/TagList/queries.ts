import { graphql } from 'gatsby'

export const FetchTagsQuery = graphql`
  fragment FetchTags on Query {
    tags: allMarkdownRemark(
      filter: { fields: { contentType: { eq: "tag" } } }
    ) {
      nodes {
        ...Tag
      }
    }
  }
`

export const CountTagsUsageQuery = graphql`
  fragment CountTagsUsage on Query {
    tagsUsage: allMarkdownRemark(
      filter: { frontmatter: { contentType: { eq: "post" } } }
    ) {
      tagsUsage: group(field: frontmatter___tags) {
        totalCount
        tagName: fieldValue
      }
    }
  }
`
