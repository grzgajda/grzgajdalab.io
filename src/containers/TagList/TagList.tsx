import React from 'react'
import { GatsbyImageProps } from 'gatsby-image'
import Tag from '../../components/Tag/Tag'
import TagList from '../../components/TagList/TagList'
import createTagList from '../../utils/createTagList'
import getImage from '../../utils/getImage'
import Spacer from './Spacer'

const RenderTags = (isActive: boolean, selectedTags: string[]) => ({
  frontmatter: { path, title, desktop, mobile, shortTitle },
}: ISingleTag) => (
  <Tag
    key={path}
    isSelected={isActive}
    title={title}
    path={selectedTags.includes(shortTitle) ? '/' : path}
    desktop={getImage(desktop)}
    mobile={getImage(mobile)}
  />
)

const TagListContainer = ({
  allTags,
  selectedTags = [],
  tagsUsage,
}: ITagListProps) => {
  const [first, last] = createTagList(allTags, selectedTags, tagsUsage)
  const allIsSelected = first.length === 0
  const withSpacer = first.length > 0 && last.length > 0

  return (
    <TagList>
      {first.map(RenderTags(true, selectedTags))}
      {withSpacer && <Spacer />}
      {last.map(RenderTags(allIsSelected, selectedTags))}
    </TagList>
  )
}

export default TagListContainer
interface ISingleTag {
  frontmatter: Omit<
    React.ComponentProps<typeof Tag>,
    'desktop' | 'mobile' | 'isSelected'
  > & {
    desktop: { childImageSharp: GatsbyImageProps } | null
    mobile: { childImageSharp: GatsbyImageProps } | null
    shortTitle: string
  }
}
interface ITagListProps {
  allTags: ISingleTag[]
  selectedTags?: string[]
  tagsUsage?: Array<{ totalCount: number; tagName: string }>
}
