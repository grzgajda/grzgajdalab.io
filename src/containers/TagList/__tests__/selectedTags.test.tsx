import React from 'react'
import { create } from 'react-test-renderer'
import Spacer from '../Spacer'
import TagListContainer from '../TagList'

describe('Container TagList has selected tags', () => {
  const makeTag = (path: string, title: string) => ({
    frontmatter: {
      desktop: null,
      mobile: null,
      path,
      shortTitle: path.replace('/', ''),
      title,
    },
  })
  const tagList = [
    makeTag('/javascript', 'Javascript'),
    makeTag('/typescript', 'TypeScript'),
  ]

  describe('all tags should be selected when list of selected tags is empty', () => {
    it('should mark all tags as non selected when "selectedTags" is undefined', () => {
      const rendered = create(<TagListContainer allTags={tagList} />)
      expect(rendered.root.findAllByProps({ isSelected: true })).toHaveLength(2)
    })

    it('should mark all tags as selected when "selectedTags" is empty', () => {
      const rendered = create(
        <TagListContainer allTags={tagList} selectedTags={[]} />
      )
      expect(rendered.root.findAllByProps({ isSelected: true })).toHaveLength(2)
    })

    it('should mark "javascript" as selected when "selectedTags" contains "javascript"', () => {
      const rendered = create(
        <TagListContainer allTags={tagList} selectedTags={['javascript']} />
      )
      expect(rendered.root.findAllByProps({ isSelected: true })).toHaveLength(1)
    })
  })

  describe('between selected and unselected tags should be greater margin', () => {
    it('should render margin when both active and unactive tags rendered', () => {
      const rendered = create(
        <TagListContainer allTags={tagList} selectedTags={['javascript']} />
      )
      expect(rendered.root.findAllByType(Spacer)).toHaveLength(3)
    })

    it('should not render margin when all tags are selected', () => {
      const rendered = create(
        <TagListContainer
          allTags={tagList}
          selectedTags={['javascript', 'typescript']}
        />
      )
      expect(rendered.root.findAllByType(Spacer)).toHaveLength(2)
    })

    it('should not render margin when all tags are unselected', () => {
      const rendered = create(<TagListContainer allTags={tagList} />)
      expect(rendered.root.findAllByType(Spacer)).toHaveLength(2)
    })
  })

  describe('tag should be clickable', () => {
    it('should redirect user to tag page when tag is not selected', () => {
      const rendered = create(<TagListContainer allTags={tagList} />)
      const tags = rendered.root.findAllByProps({
        path: '/javascript',
      })

      expect(tags).toHaveLength(1)
    })

    it('should redirect to tag when list of selected tags is empty', () => {
      const rendered = create(
        <TagListContainer allTags={tagList} selectedTags={[]} />
      )
      const tags = rendered.root.findAllByProps({
        path: '/javascript',
      })

      expect(tags).toHaveLength(1)
    })

    it('should redirect to homepage when tag is selected', () => {
      const rendered = create(
        <TagListContainer allTags={tagList} selectedTags={['javascript']} />
      )
      const tags = rendered.root.findAllByProps({
        path: '/javascript',
      })

      expect(tags).toHaveLength(0)
    })

    it('should redirect to tag page when one tag is selected and another not', () => {
      const rendered = create(
        <TagListContainer allTags={tagList} selectedTags={['javascript']} />
      )
      const tags = rendered.root.findAllByProps({
        path: '/typescript',
      })

      expect(tags).toHaveLength(1)
    })
  })
})
