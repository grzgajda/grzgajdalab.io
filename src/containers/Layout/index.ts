import Layout from './Layout'
import QueryLayout from './QueryLayout'

export { Layout, QueryLayout }
export default QueryLayout(Layout)
