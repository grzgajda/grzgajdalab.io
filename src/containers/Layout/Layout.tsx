import { Link } from 'gatsby'
import React from 'react'
import Helmet from 'react-helmet'
import Logo from '../../components/Logo'
import AppleIcon from '../AppleIcon'
import { GlobalStyles } from '../../theme'

const Layout = ({
  children,
  siteLogo,
  siteName,
}: ILayoutProps & ILayoutMeta) => (
  <React.Fragment>
    <Helmet>
      <html lang={'pl'} />
      <link rel={'icon'} type={'image/png'} href={siteLogo} />
    </Helmet>
    <AppleIcon />
    <GlobalStyles />
    <Link to={'/'}>
      <Logo siteName={siteName} iconSrc={siteLogo} withBorder={true} />
    </Link>

    {children}
  </React.Fragment>
)

interface ILayoutMeta {
  siteName: string
  siteLogo: string
}
interface ILayoutProps {
  children: React.ReactNode
  language?: string
}

export default Layout
