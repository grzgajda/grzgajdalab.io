import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import Layout from './Layout'

const QueryLayout = (Component: typeof Layout) => ({
  children,
}: IQueryWrapper) => (
  <StaticQuery
    query={graphql`
      query LayoutMetadata {
        site {
          siteMetadata {
            title
          }
        }
        logo: file(relativePath: { regex: "/logo.svg/" }) {
          publicURL
        }
      }
    `}
    render={({ site, logo }: IQueryResult) => (
      <Component siteLogo={logo.publicURL} siteName={site.siteMetadata.title}>
        {children}
      </Component>
    )}
  />
)

interface IQueryWrapper {
  children?: React.ReactNode
}
interface IQueryResult {
  site: {
    siteMetadata: {
      title: string
    }
  }
  logo: {
    publicURL: string
  }
}

export default QueryLayout
