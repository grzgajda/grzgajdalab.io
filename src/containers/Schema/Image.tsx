import React from 'react'
import { GatsbyImageProps, FixedObject, FluidObject } from 'gatsby-image'
import { StaticQuery, graphql } from 'gatsby'

const Image = ({ imageUrl, siteUrl }: IImageUrl) => {
  if (!imageUrl) {
    return <React.Fragment />
  }

  if (imageUrl.fixed) {
    const image = imageUrl.fixed as FixedObject
    return <meta itemProp={'image'} content={`${siteUrl}${image.src}`} />
  }
  if (imageUrl.fluid) {
    const image = imageUrl.fluid as FluidObject
    return <meta itemProp={'image'} content={`${siteUrl}${image.src}`} />
  }

  return <React.Fragment />
}

const QueryImage = ({ imageUrl }: Pick<IImageUrl, 'imageUrl'>) => (
  <StaticQuery
    query={graphql`
      query HomepageUrl {
        site {
          siteMetadata {
            siteUrl
          }
        }
      }
    `}
    render={({ site }: IQueryResult) => (
      <Image imageUrl={imageUrl} siteUrl={site.siteMetadata.siteUrl} />
    )}
  />
)

interface IImageUrl {
  imageUrl: GatsbyImageProps | undefined
  siteUrl: string
}
interface IQueryResult {
  site: {
    siteMetadata: {
      siteUrl: string
    }
  }
}

export default QueryImage
