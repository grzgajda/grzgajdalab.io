import React from 'react'

const Person = () => (
  <div itemScope itemProp={'author'} itemType={'https://schema.org/Person'}>
    <meta itemProp={'jobTitle'} content={'Software Developer'} />
    <meta itemProp={'name'} content={'Grzegorz Gajda'} />
  </div>
)

export default Person
