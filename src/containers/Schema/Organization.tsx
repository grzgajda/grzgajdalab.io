import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

const Organization = () => (
  <StaticQuery
    query={graphql`
      query SchemaOrganization {
        logo: file(relativePath: { regex: "/logo.svg/" }) {
          publicURL
        }
        site {
          siteMetadata {
            siteUrl
            title
          }
        }
      }
    `}
    render={({ site, logo }: IQueryResult) => (
      <div
        itemScope
        itemProp={'publisher'}
        itemType="http://schema.org/Organization"
      >
        <meta itemProp={'name'} content={site.siteMetadata.title} />
        <meta itemProp={'url'} content={site.siteMetadata.siteUrl} />
        <div itemScope itemProp={'logo'} itemType={'https://schema.org/URL'}>
          <meta
            itemProp={'url'}
            content={`${site.siteMetadata.siteUrl}${logo.publicURL}`}
          />
        </div>
      </div>
    )}
  />
)

interface IQueryResult {
  logo: {
    publicURL: string
  }
  site: {
    siteMetadata: {
      siteUrl: string
      title: string
    }
  }
}

export default Organization
