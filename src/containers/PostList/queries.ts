import { graphql } from 'gatsby'

export const AllPostsQuery = graphql`
  fragment AllPosts on Query {
    posts: allMarkdownRemark(
      sort: { fields: frontmatter___date, order: DESC }
      filter: { fields: { contentType: { eq: "post" } } }
    ) {
      nodes {
        ...PostPreview
      }
    }
  }
`
