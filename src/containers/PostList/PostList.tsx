import React from 'react'
import { GatsbyImageProps } from 'gatsby-image'
import Preview from '../../components/Preview'
import PreviewList from '../../components/PreviewList/PreviewList'
import getImage from '../../utils/getImage'

const PostList = ({ allPosts }: IPostListProps) => (
  <PreviewList>
    {allPosts.map(({ frontmatter, excerpt }) => (
      <Preview
        key={frontmatter.path}
        title={frontmatter.title}
        path={frontmatter.path}
        desktop={getImage(frontmatter.desktop)}
        mobile={getImage(frontmatter.mobile)}
        excerpt={frontmatter.intro || excerpt}
      />
    ))}
  </PreviewList>
)

export default PostList
interface IPostListProps {
  allPosts: Array<{
    frontmatter: Omit<
      React.ComponentProps<typeof Preview>,
      'excerpt' | 'desktop' | 'mobile'
    > & {
      desktop: { childImageSharp?: GatsbyImageProps }
      mobile: { childImageSharp?: GatsbyImageProps }
      intro?: string
    }
    excerpt: string
  }>
}
