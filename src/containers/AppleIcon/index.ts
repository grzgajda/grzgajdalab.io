import AppleIcon from './AppleIcon'
import QueryIcon from './QueryIcon'

export { AppleIcon, QueryIcon }
export default QueryIcon(AppleIcon)
