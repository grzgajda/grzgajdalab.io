import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import AppleIcon from './AppleIcon'

const QueryIcon = (Component: typeof AppleIcon) => () => (
  <StaticQuery
    query={graphql`
      query AppleIconComponent {
        site {
          siteMetadata {
            metaTitle
          }
        }
        iphone: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/iphone/" }
        ) {
          publicURL
        }
        iphoneR: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/iphone-retina/" }
        ) {
          publicURL
        }
        ipadPortrait: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/ipad-portrait/" }
        ) {
          publicURL
        }
        ipadLandscape: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/ipad-landscape/" }
        ) {
          publicURL
        }
        ipadRetinaP: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/ipad-retina-portrait/" }
        ) {
          publicURL
        }
        ipadRetinaL: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/ipad-retina-landscape/" }
        ) {
          publicURL
        }
        iphone678: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/iphone-6-7-8.png/" }
        ) {
          publicURL
        }
        iphone678plus: file(
          relativeDirectory: { regex: "/apple-/" }
          relativePath: { regex: "/iphone-6-7-8-plus/" }
        ) {
          publicURL
        }
      }
    `}
    render={Component}
  />
)

export default QueryIcon
