import React from 'react'
import Helmet from 'react-helmet'

const RenderComponent = ({ site, ...images }: IAppleIcon & ISiteMetadata) => (
  <Helmet>
    <meta
      name={'apple-mobile-web-app-title'}
      content={site.siteMetadata.metaTitle}
    />
    <meta name={'apple-mobile-web-app-capable'} content={'yes'} />

    {/* Apple splash screens */}
    <link
      rel={'apple-touch-startup-image'}
      href={images.iphone.publicURL}
      media={'(device-width: 320px)'}
    />
    <link
      rel={'apple-touch-startup-image'}
      href={images.iphoneR.publicURL}
      media={'(device-width: 320px) and (-webkit-device-pixel-ratio: 2)'}
    />
    <link
      rel={'apple-touch-startup-image'}
      href={images.ipadPortrait.publicURL}
      media={'(device-width: 768px) and (orientation: portrait)'}
    />
    <link
      rel={'apple-touch-startup-image'}
      href={images.ipadLandscape.publicURL}
      media={'(device-width: 768px) and (orientation: landscape)'}
    />
    <link
      rel={'apple-touch-startup-image'}
      href={images.ipadRetinaP.publicURL}
      media={
        '(device-width: 1536px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)'
      }
    />
    <link
      rel={'apple-touch-startup-image'}
      href={images.ipadRetinaL.publicURL}
      media={
        '(device-width: 1536px)  and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)'
      }
    />
    <link
      rel={'apple-touch-startup-image'}
      href={images.iphone678.publicURL}
      media={'(device-width: 375px) and (-webkit-device-pixel-ratio: 2)'}
    />
    <link
      rel={'apple-touch-startup-image'}
      href={images.iphone678plus.publicURL}
      media={'(device-width: 414px) and (-webkit-device-pixel-ratio: 3)'}
    />
  </Helmet>
)

interface IFileType {
  publicURL: string
}
interface ISiteMetadata {
  site: {
    siteMetadata: {
      metaTitle: string
    }
  }
}
interface IAppleIcon {
  iphone: IFileType
  iphoneR: IFileType
  ipadPortrait: IFileType
  ipadLandscape: IFileType
  ipadRetinaP: IFileType
  ipadRetinaL: IFileType
  iphone678: IFileType
  iphone678plus: IFileType
}

export default RenderComponent
