import { graphql } from 'gatsby'

export const SiteMetadataQuery = graphql`
  fragment SiteMetadata on Query {
    site {
      siteMetadata {
        title
        metaTitle
        metaDescription
        siteUrl
      }
    }
  }
`
