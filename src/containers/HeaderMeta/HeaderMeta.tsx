import React from 'react'
import Helmet from 'react-helmet'

const HeaderMeta = ({
  siteMetadata,
  title,
  description,
  canonicalUrl,
}: IHeaderMetaProps & IHeaderMetaQuery) => (
  <Helmet>
    <title>
      {title ? `${title} - ${siteMetadata.title}` : siteMetadata.metaTitle}
    </title>
    <meta
      name={'description'}
      content={description || siteMetadata.metaDescription}
    />
    {canonicalUrl && (
      <link rel={'canonical'} href={`${siteMetadata.siteUrl}${canonicalUrl}`} />
    )}
  </Helmet>
)

export default HeaderMeta
interface IHeaderMetaQuery {
  siteMetadata: {
    title: string
    metaTitle: string
    metaDescription: string
    siteUrl: string
  }
}
interface IHeaderMetaProps {
  title?: string
  description?: string
  canonicalUrl?: string
}
