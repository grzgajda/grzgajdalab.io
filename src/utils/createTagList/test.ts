import Tag from '../../containers/TagList'
import createTagList from './index'

describe('Function to create sorted tag lists', () => {
  const tags: React.ComponentProps<typeof Tag>['allTags'] = [
    {
      frontmatter: {
        desktop: null,
        mobile: null,
        path: '/tags/docker',
        shortTitle: 'docker',
        title: 'Docker',
      },
    },
    {
      frontmatter: {
        desktop: null,
        mobile: null,
        path: '/tags/styled-components',
        shortTitle: 'styled-components',
        title: 'styled-components',
      },
    },
  ]

  test('when list of selected tags is empty, return all tags as selected', () => {
    const [first, last] = createTagList(tags)
    expect(first).toHaveLength(2)
    expect(last).toHaveLength(0)
  })

  it('should return two lists ordered, the first contains only active tags', () => {
    const [first, last] = createTagList(tags, ['docker'])
    expect(first).toHaveLength(1)
    expect(last).toHaveLength(1)
  })

  it('should return two lists ordered and second list should contains all tags when selected tags is empty', () => {
    const [first, last] = createTagList(tags, [])
    expect(first).toHaveLength(0)
    expect(last).toHaveLength(2)
  })

  test('second list should contains all tags when selected tag does not exists', () => {
    const [first, last] = createTagList(tags, ['lorem ipsum'])
    expect(first).toHaveLength(0)
    expect(last).toHaveLength(2)
  })

  test('first list should contains all tags when selected tags equals to all tags', () => {
    const [first, last] = createTagList(tags, ['docker', 'styled-components'])
    expect(first).toHaveLength(2)
    expect(last).toHaveLength(0)
  })

  it('should returns an array when styled-components is first because has more articles', () => {
    const ordered = [
      { tagName: 'docker', totalCount: 1 },
      { tagName: 'styled-components', totalCount: 3 },
    ]
    const [first, last] = createTagList(
      tags,
      ['docker', 'styled-components'],
      ordered
    )
    expect(first).toHaveLength(2)
    expect(last).toHaveLength(0)
    expect(first[0].frontmatter.title).toEqual('styled-components')
  })
})

describe('Function to create sorted tag lists with new "shortTitle"', () => {
  const createTag = (title: string, short: string, path: string) => ({
    frontmatter: { path, shortTitle: short, title },
  })
  const tags = [
    createTag('Docker', 'docker', '/tags/docker'),
    createTag('styled-components', 'styled-components', '/tags/docker'),
    createTag('Node.js', 'nodejs', '/tags/nodejs'),
  ]

  it('should match Node.js as used tag', () => {
    const [first, last] = createTagList(tags, ['nodejs'])
    expect(first).toHaveLength(1)
    expect(last).toHaveLength(2)
  })
})

describe('orderd tags by posts count', () => {
  const createTag = (title: string, short: string, path: string) => ({
    frontmatter: { path, shortTitle: short, title },
  })
  const tags = [
    createTag('Docker', 'docker', '/docker'),
    createTag('Node.js', 'nodejs', '/nodejs'),
    createTag('PHP', 'php', '/php'),
    createTag('Golang', 'golang', '/golang'),
  ]

  it('should reorder tags by posts counter', () => {
    const [first] = createTagList(
      tags,
      ['docker', 'nodejs', 'php', 'golang'],
      [
        { tagName: 'docker', totalCount: 1 },
        { tagName: 'nodejs', totalCount: 4 },
        { tagName: 'php', totalCount: 3 },
        { tagName: 'golang', totalCount: 2 },
      ]
    )

    expect(first[0].frontmatter.title).toEqual('Node.js')
    expect(first[1].frontmatter.title).toEqual('PHP')
    expect(first[2].frontmatter.title).toEqual('Golang')
    expect(first[3].frontmatter.title).toEqual('Docker')
  })
})
