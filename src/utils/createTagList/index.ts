interface ITag {
  frontmatter: {
    title: string
    path: string
    shortTitle?: string
  }
}

interface IOrderTag {
  totalCount: number
  tagName: string
}

function orderTags(order: IOrderTag[]): { [key: string]: number } {
  const results: { [key: string]: number } = {}
  order.forEach(o => {
    results[o.tagName] = o.totalCount
  })

  return results
}

export default function<P extends ITag>(
  tags: P[],
  selected?: string[],
  order?: IOrderTag[]
): [P[], P[]] {
  const orderedSum = orderTags(order || [])
  let orderedTags = tags

  if ({} !== orderedSum) {
    orderedTags = orderedTags.sort((a, b) => {
      const aTitle =
        a.frontmatter.shortTitle || a.frontmatter.title.toLowerCase()
      const bTitle =
        b.frontmatter.shortTitle || b.frontmatter.title.toLowerCase()

      return orderedSum[aTitle] > orderedSum[bTitle] ? -1 : 1
    })
  }

  const first =
    selected && null !== selected
      ? orderedTags.filter(
          t =>
            selected.indexOf(
              t.frontmatter.shortTitle || t.frontmatter.title.toLowerCase()
            ) >= 0
        )
      : orderedTags

  const last =
    selected && null !== selected
      ? orderedTags.filter(
          t =>
            selected.indexOf(
              t.frontmatter.shortTitle || t.frontmatter.title.toLowerCase()
            ) === -1
        )
      : []

  return [first, last]
}
