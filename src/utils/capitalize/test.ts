import capitalize from './index'

describe('capitalize utility for strings', () => {
  it('should replace "lorem ipsum" to "Lorem ipsum"', () => {
    const result = capitalize('lorem ipsum')
    expect(result).toEqual('Lorem ipsum')
  })

  it('should do nothing for "Lorem ipsum"', () => {
    const result = capitalize('Lorem ipsum')
    expect(result).toEqual('Lorem ipsum')
  })
})
