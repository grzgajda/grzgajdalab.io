import { GatsbyImageProps } from 'gatsby-image'

interface IContainsImage {
  childImageSharp?: GatsbyImageProps
}

const getImage = (image: IContainsImage | null): GatsbyImageProps | undefined =>
  null !== image ? image && image.childImageSharp : undefined

export default getImage
