import { GatsbyImageProps } from 'gatsby-image'
import getImage from './index'

describe('Function to get path for gatsby-image', () => {
  it('should return an undefined when object is empty', () => {
    const result = getImage({})
    expect(result).toBeUndefined()
  })

  it('should return an undefined when object is null', () => {
    const result = getImage(null)
    expect(result).toBeUndefined()
  })

  it('should return an attribute when it is not empty', () => {
    const chi: GatsbyImageProps = {}
    const result = getImage({
      childImageSharp: chi,
    })

    expect(result).toBe(chi)
  })
})
