const textTruncate = (text: string, length = 140): string => {
  if (text.length <= length) {
    return text
  }

  const spacePosition = text.substr(length).indexOf(' ')
  if (-1 === spacePosition) {
    return `${text}...`
  }

  return `${text.substring(0, length + spacePosition)}...`
}

export default textTruncate
