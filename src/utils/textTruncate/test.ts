import textTruncate from './index'

describe('Function to truncate text and add ellipsis on the end', () => {
  it('should remain original text when contains less characters than length', () => {
    const result = textTruncate('ABC', 10)
    expect(result).toEqual('ABC')
  })

  it('should remain original string when text has equal characters to length', () => {
    const result = textTruncate('ABC', 3)
    expect(result).toEqual('ABC')
  })

  it('should have remain 140 symbols by default', () => {
    const result = textTruncate('ABC')
    expect(result).toEqual('ABC')
  })

  it('should not trim words', () => {
    const result = textTruncate('ABCDEFGHIJKLMN', 3)
    expect(result).toEqual('ABCDEFGHIJKLMN...')
  })

  it('should trim sentences from next word', () => {
    const result = textTruncate('Lorem ipsum dolor sit amet', 3)
    expect(result).toEqual('Lorem...')
  })
})
