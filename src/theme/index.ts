import * as styledComponents from 'styled-components'

const {
  default: styled,
  css,
  keyframes,
  ThemeProvider,
  createGlobalStyle,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<{}>

const GlobalStyles = createGlobalStyle`
  :root {
    --colors-light: rgb(46, 170, 170);
    --colors-border: rgb(249, 249, 249);

    --text-font: Montserrat, sans-serif;
    --text-color: rgb(100, 100, 100);
    --header-color: rgb(65, 65, 65);
    --image-shadow: 0px 7px 15px rgba(221, 221, 221);
    --image-radius: 10px;

    --container-width: 700px;
    --container-small-margin: 20px;
    --container-margin: 50px;
    --tag-size: 50px;
    --post-width: 335px;
    --post-height: 175px;

    @media (prefers-color-scheme: dark) {
      --colors-border: #18171b;
      --colors-background: #36393e;
      --header-color: #aaa;
      --text-color: silver;
      --image-shadow: 0px 7px 15px #2e3136;
    }

    @media (min-width: 600px) {
      --tag-size: 100px;
    }
  }

  html, body {
    margin: 0;
    padding: 0;
    background-color: var(--colors-background);
  }

  * {
    font-family: var(--text-font);
    box-sizing: border-box;
  }
  h1, h2, h3, h4, h5, h6 {
    font-family: var(--text-font);
  }
  pre,
  pre * {
    font-family: Consolas, Monaco, "Andale Mono", monospace;
    font-display: fallback;
    text-align: left;
    font-size: 14px;
  }
  pre {
    margin: 30px auto;
  }
  a,
  a:focus,
  a:visited {
    color: var(--colors-light);
    text-decoration: none;
  }
  
  .remark-draw.remark-draw-mermaid {
    &,
    & > svg {
      .node rect {
        fill: white !important;
        stroke: var(--header-color) !important;
      }
      .node .label {
        color: var(--text-color) !important;
        font-size: 16px;
      }
      .edgeLabel {
        background-color: var(--colors-background) !important;
        font-size: 14px;
      }
      .edgePath .path {
        stroke: var(--colors-light) !important;
      }
      .edgePath .arrowheadPath {
        fill: var(--colors-light) !important;
      }
    }
  }
`

const GoogleFonts = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,700,900|Radley&display=swap');
`

export default styled
export { css, keyframes, ThemeProvider, GlobalStyles, GoogleFonts }
