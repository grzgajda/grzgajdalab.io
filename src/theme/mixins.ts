import { css } from '.'

export const shadowed = css`
  max-width: 100%;

  margin-top: var(--container-small-margin);
  margin-bottom: var(--container-small-margin);
  @media (min-width: 768px) {
    margin-top: var(--container-margin);
    margin-bottom: var(--container-margin);
  }

  border-radius: var(--image-radius);
  box-shadow: var(--image-shadow);
`
