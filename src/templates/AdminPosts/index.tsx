import 'react-table/react-table.css'
import React from 'react'
import Table from 'react-table'
import { graphql } from 'gatsby'
import { GatsbyImageProps } from 'gatsby-image'
import TagList from '../../containers/TagList'
import { Wrapper } from './styles'
import {
  CoverColumn,
  TitleColumn,
  PublishDateColumn,
  TimeToReadColumn,
  WordCountColumn,
  IntroColumn,
  TagsColumn,
} from './columns'

const PostsData = ({ data }: IIndexPageProps) => (
  <Wrapper>
    <Table
      columns={[
        CoverColumn(),
        TitleColumn(),
        TagsColumn(),
        PublishDateColumn(),
        IntroColumn(),
        TimeToReadColumn(),
        WordCountColumn(),
      ]}
      defaultPageSize={data.posts.nodes.length}
      data={data.posts.nodes}
      showPageJump={false}
      showPagination={false}
    />
  </Wrapper>
)

export default PostsData

interface IIndexPageProps {
  data: {
    tags: {
      nodes: React.ComponentProps<typeof TagList>['allTags']
    }
    tagsUsage: Array<{ totalCount: number; tagName: string }>
    posts: {
      nodes: Array<{
        frontmatter: {
          contentType: string
          date: string
          intro: string
          language: string
          path: string
          seoDescription: string
          tags: string[]
          title: string
          cover: null | { id: string } & { childImageSharp: GatsbyImageProps }
        }
        timeToRead: number
        wordCount: {
          words: number
          sentences: number
          paragraphs: number
        }
      }>
    }
  }
}

export const query = graphql`
  query PostsDataPage {
    posts: allMarkdownRemark(
      filter: { fields: { contentType: { eq: "post" } } }
      sort: { fields: frontmatter___date, order: DESC }
    ) {
      nodes {
        frontmatter {
          contentType
          date(formatString: "DD-MM-YYYY")
          intro
          language
          path
          seoDescription
          tags
          title
          cover {
            id
            ...AdminPreviewCover
          }
        }
        timeToRead
        wordCount {
          words
          sentences
          paragraphs
        }
      }
    }
  }
`
