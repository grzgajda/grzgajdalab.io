import { Column } from 'react-table'

export const sortMethod = (a: string | null, b: string | null): number => {
  const aValue = a ? a.length : 0
  const bValue = b ? b.length : 0

  return aValue > bValue ? 1 : -1
}

const column = (): Column => ({
  Cell: ({ value }: ICoverColumn) => (value ? value.length : 0),
  Header: 'Długość opisu',
  accessor: 'frontmatter.intro',
  sortMethod,
})

export default column

interface ICoverColumn {
  value: string
}
