import CoverColumn from './cover'
import TitleColumn from './title'
import PublishDateColumn from './publishDate'
import TimeToReadColumn from './timeToRead'
import WordCountColumn from './wordCount'
import IntroColumn from './intro'
import TagsColumn from './tags'

export {
  CoverColumn,
  TitleColumn,
  PublishDateColumn,
  TimeToReadColumn,
  WordCountColumn,
  IntroColumn,
  TagsColumn,
}
