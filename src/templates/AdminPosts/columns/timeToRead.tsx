import { Column } from 'react-table'

const column = (): Column => ({
  Cell: ({ value }: IColumnValue) => `${value} m`,
  Header: 'Czas czytania',
  accessor: 'timeToRead',
})

export default column
interface IColumnValue {
  value: number
}
