import { Column } from 'react-table'

const column = (): Column => ({
  Header: 'Liczba słów',
  accessor: 'wordCount.words',
})

export default column
