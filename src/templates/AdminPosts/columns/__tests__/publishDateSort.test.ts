import { sortMethod } from '../publishDate'

describe('Column "date" sorting method', () => {
  it('should return 1 for earlier date', () => {
    const result = sortMethod('01-01-2001', '01-01-2000')
    expect(result).toEqual(1)
  })
})
