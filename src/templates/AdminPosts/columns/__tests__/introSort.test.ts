import { sortMethod } from '../intro'

describe('Column "intro" sorting method', () => {
  it('should sort by length of text', () => {
    const result = sortMethod('ABC', 'AA')
    expect(result).toEqual(1)
  })

  it('should treat null as a 0', () => {
    const result = sortMethod('ABC', null)
    expect(result).toEqual(1)
  })
})
