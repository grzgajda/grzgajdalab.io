import React from 'react'
import { Column } from 'react-table'
import { GatsbyImageProps } from 'gatsby-image'
import Cover from '../../../components/Cover'

const CoverRenderer = ({ value }: ICoverColumn) => {
  return value && <Cover withoutShadow={true} cover={value.childImageSharp} />
}

const column = (): Column => ({
  Cell: CoverRenderer,
  Header: 'Okładka',
  accessor: 'frontmatter.cover',
})

export default column

interface ICoverColumn {
  value: null | {
    childImageSharp: GatsbyImageProps
  }
}
