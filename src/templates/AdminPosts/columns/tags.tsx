import { Column } from 'react-table'

const column = (): Column => ({
  Cell: ({ value }: IValue) => (value ? value.join(', ') : '---'),
  Header: 'Tagi',
  accessor: 'frontmatter.tags',
})

export default column
interface IValue {
  value: string[]
}
