import { Column } from 'react-table'

const column = (): Column => ({
  Header: 'Tytuł',
  accessor: 'frontmatter.title',
})

export default column
