import { Column } from 'react-table'

const sortMethod = (a: string, b: string): number => {
  const [aDay, aMonth, aYear] = a.split('-').map(a => parseInt(a, 10))
  const [bDay, bMonth, bYear] = b.split('-').map(b => parseInt(b, 10))

  return new Date(aYear, aMonth - 1, aDay) >= new Date(bYear, bMonth - 1, bDay)
    ? 1
    : -1
}

const column = (): Column => ({
  Header: 'Data publikacji',
  accessor: 'frontmatter.date',
  sortMethod,
})

export default column
export { sortMethod }
