import { graphql } from 'gatsby'
import React from 'react'
import TagList from '../../containers/TagList'
import HeaderMeta from '../../containers/HeaderMeta'
import PostList from '../../containers/PostList'

type TagBrowserProps = IQueryInterface & IRouteInterface

const TagBrowser = ({ data, pathContext }: TagBrowserProps) => (
  <React.Fragment>
    <HeaderMeta
      siteMetadata={data.site.siteMetadata}
      title={pathContext.tagName}
      description={pathContext.seoDescription}
      canonicalUrl={pathContext.slug}
    />
    <TagList
      allTags={data.tags.nodes}
      tagsUsage={data.tagsUsage.tagsUsage}
      selectedTags={[pathContext.slugName]}
    />
    <PostList allPosts={data.posts.nodes} />
  </React.Fragment>
)

export default TagBrowser
export interface IRouteInterface {
  pathContext: {
    tagName: string
    slugName: string
    seoDescription: string
    slug: string
  }
}
export interface IQueryInterface {
  data: {
    tags: {
      nodes: React.ComponentProps<typeof TagList>['allTags']
    }
    tagsUsage: {
      tagsUsage: Array<{ totalCount: number; tagName: string }>
    }
    posts: {
      nodes: React.ComponentProps<typeof PostList>['allPosts']
    }
    site: React.ComponentProps<typeof HeaderMeta>
  }
}

export const pageQuery = graphql`
  query($slugName: String!) {
    ...FetchTags
    ...CountTagsUsage
    ...SiteMetadata
    posts: allMarkdownRemark(
      filter: {
        fields: { contentType: { eq: "post" }, tags: { in: [$slugName] } }
      }
      sort: { fields: frontmatter___date, order: DESC }
    ) {
      nodes {
        ...PostPreview
      }
    }
  }
`
