import { graphql } from 'gatsby'
import { GatsbyImageProps } from 'gatsby-image'
import React from 'react'
import BlogPostContent from '../../components/BlogPostContent'
import BlogPostContainer from '../../components/BlogPost'
import HeaderMeta from '../../containers/HeaderMeta'
import TagListContainer from '../../containers/TagList'
import getImage from '../../utils/getImage'

type BlogPostProps = IQueryInterface & IRouteInterface

const BlogPost = ({ data, pageContext }: BlogPostProps) => (
  <React.Fragment>
    <HeaderMeta
      siteMetadata={data.site.siteMetadata}
      title={data.markdownRemark.frontmatter.title}
      description={data.markdownRemark.frontmatter.intro}
      canonicalUrl={pageContext.slug}
    />
    <TagListContainer
      allTags={data.tags.nodes}
      selectedTags={pageContext.tags || []}
      tagsUsage={data.tagsUsage.tagsUsage}
    />
    <BlogPostContainer
      title={data.markdownRemark.frontmatter.title}
      cover={getImage(data.markdownRemark.frontmatter.cover)}
      introduction={data.markdownRemark.frontmatter.intro}
      publishedAt={data.markdownRemark.frontmatter.publishedAt}
    >
      <BlogPostContent>{data.markdownRemark.content}</BlogPostContent>
    </BlogPostContainer>
  </React.Fragment>
)

export default BlogPost
interface IRouteInterface {
  pageContext: {
    slug: string
    title: string
    description: string
    tags: string[] | null
  }
}
interface IQueryInterface {
  data: {
    tags: {
      nodes: React.ComponentProps<typeof TagListContainer>['allTags']
    }
    tagsUsage: {
      tagsUsage: Array<{ totalCount: number; tagName: string }>
    }
    site: React.ComponentProps<typeof HeaderMeta>
    markdownRemark: {
      content: string
      frontmatter: {
        title: string
        path: string
        tags: string[]
        intro?: string
        publishedAt: string
        cover: null | {
          childImageSharp: GatsbyImageProps
        }
      }
    }
  }
}

export const pageQuery = graphql`
  query($slug: String!) {
    ...FetchTags
    ...SiteMetadata
    ...CountTagsUsage
    markdownRemark(fields: { slug: { eq: $slug } }) {
      ...BlogPost
    }
  }
`
