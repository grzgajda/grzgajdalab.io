import { graphql } from 'gatsby'
import React from 'react'
import HeaderMeta from '../containers/HeaderMeta/HeaderMeta'
import TagList from '../containers/TagList'
import PostList from '../containers/PostList'

const IndexPage = ({ data }: IIndexPageProps) => (
  <React.Fragment>
    <HeaderMeta siteMetadata={data.site.siteMetadata} />

    <TagList
      allTags={data.tags.nodes}
      selectedTags={[]}
      tagsUsage={data.tagsUsage.tagsUsage}
    />
    <PostList allPosts={data.posts.nodes} />
  </React.Fragment>
)

export default IndexPage
interface IIndexPageProps {
  data: {
    tags: {
      nodes: React.ComponentProps<typeof TagList>['allTags']
    }
    tagsUsage: {
      tagsUsage: Array<{ totalCount: number; tagName: string }>
    }
    posts: {
      nodes: React.ComponentProps<typeof PostList>['allPosts']
    }
    site: React.ComponentProps<typeof HeaderMeta>
  }
}

export const query = graphql`
  query Homepage {
    ...FetchTags
    ...CountTagsUsage
    ...AllPosts
    ...SiteMetadata
  }
`
