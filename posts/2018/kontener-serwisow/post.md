---
path: '/kontener-serwisow'
date: '2018-12-24'
title: 'Kontener serwisów'
contentType: 'post'
tags: ['php']
language: 'pl'
cover: './cover.png'
intro: 'Każdy programista chce by jego aplikacja się rozrastała. A z każdym rozwojem rośnie liczba linii kodu, wzrasta liczba testów i projekt coraz ciężej utrzymywać. Jedną z pomocy do organizacji projektu jest kontener serwisów (service container).'
---

Jest to _”tablica”_ obiektów, gdzie pod odpowiednim kluczem znajduje się odpowiedni serwis. W PHP taki kontener jest tak dobrze przyjętą praktyką, że doczekał się szczegółowego opisania przez nieoficjalne standardy, tzw. [PHP-FIG](https://www.php-fig.org/psr/psr-11/meta/).

```php
<?php declare(strict_types=1);
namespace Psr\Container;

/**
 * Describes the interface of a container that exposes methods to read its entries.
 */
interface ContainerInterface
{
    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id);

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has($id);
}
```

[PSR-11](https://www.php-fig.org/psr/psr-11/) bardzo szczegółowo opisuje wady i zalety takiego rozwiązania oraz sposoby korzystania z takiego _kontenera serwisów_. Przed zastosowaniem takiego kontenera polecam zastanowić się w jakich sytuacjach można go zastosować, jakie są ograniczenia oraz co robić by nie „zabrudzić” projektu.

## Nie chowaj swoich zależności!

Wszystkie serwisy w aplikacji powinny komunikować się między sobą za pomocą interfejsów, a zależności do serwisu powinny być definiowane w konstruktorze. Liczba zależności nie powinna przekraczać pewnej, określonej liczby, np. 4. Ok, ale jak możemy schować zależności?

Chcemy napisać kontroler do obsługi użytkownika. Będzie nam potrzebne repozytorium do zapisywania zmian, serwis do walidacji danych wejściowych, może coś do tworzenia widoków.

```php
<?php declare(strict_types=1);

class UserController
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function showUsers()
    {
        return $this->container->get('htmlRenderer')->render('your_template_name', [
            'users' => $this->container->get('users-repository')->findAll(),
        ])
    }
}
```

Jak można zauważyć, bezpośrednie korzystanie z kontenera prowadzi do chowania zależności. Nie wiemy co nam jest potrzebne do utworzenia klasy `UserController`. Dodatkowo `ContainerInterface` nie oferuje typowania obiektów więc nawet nie mamy pewności czy obiekt schowany pod kluczem _htmlRenderer_ posiada publiczną metodę `render`, a jeżeli posiada to jakie argumenty przyjmuje ta metoda.

Dodatkowym utrudnieniem jest napisanie testu dla metody `showUsers()`. Tak naprawdę nie jesteśmy pewni co nam zwróci kontener, należy również przewidzieć skutki uboczne, pod kluczym _htmlRenderer_ może akurat nie być żadnego obiektu. Należałoby napisać kod sprawdzający czy kontener posiada pod danym kluczem konkretny obiekt (lub implementację interfejsu).

```php
<?php declare(strict_types=1);

class UserControllerTest extends TestCase
{
    public function testController()
    {
        $container = $this->createMock(ContainerInterface::class);
        $container->method('get', 'htmlRenderer')->willReturn(/* what it should return? */)

        $controller = new UserController($container);
        $controller->showUsers();
    }
}
```

## Fabryki

Bardzo dobrym miejscem na zastosowanie wzorca _service locator_ są fabryki. Fabryka jest wzorcem projektowym stosowanym do tworzenia serwisów. Taka klasa zapobiega duplikowaniu kodu za każdym razem jak chcemy stworzyć nową instancję danego serwisu.

```php
<?php declare(strict_types=1);

class UserControllerFactory
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create()
    {
        return new UserController(
            $this->container->get('userRepository'),
            $this->container->get('htmlRenderer')
        );
    }
}
```

Cała logika do walidacji kontenera jest zamknięta w jednym miejscu, a klasa `UserController` posiada otwarte zależności. Taki manewr pozwala na zastosowanie wzorca _Dependency Injection (wstrzykiwanie zależności)_. Nasza klasa kontrolera wie dokładnie czego potrzebuje, a implementacją tych zależności zajmuje się fabryka.
Wzorce **Service Locator** oraz **Dependency Injection** służą do tego samego jednakże działają w całkowicie inny sposób. Oba wzorce są stosowane od oddzielenia logiki aplikacji od aplikacji przez _luźne powiązania_. Jednakże rozwiązują ten sam problem w inny sposób.

## Uwaga na nazewnictwo

W momencie pisania tego artykułu zauważyłem pewną nieścisłość w nazewnictwie. Bardzo często podczas czytania źródeł nt. kontenera serwisów widziałem naprzemienne stosowanie nazwy _service container_ oraz _service locator_. Należy wiedzieć, że pierwsza nazwa odnosi się do klasy zawierającej wszystkie obiekty pod odpowiednimi kluczami gdy druga nazwa to antywzorzec, tłumaczący jak stosować taki kontener.
