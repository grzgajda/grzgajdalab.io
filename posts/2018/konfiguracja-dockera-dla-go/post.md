---
path: '/konfiguracja-dockera-dla-go'
date: '2018-12-30'
title: 'Konfiguracja Dockera dla Go'
contentType: 'post'
tags: ['golang', 'docker']
language: 'pl'
cover: './cover.png'
intro: |
  By pracować wygodnie z kompilowanym językiem należy posiadać narzędzia, które nam taką pracę ułatwią. W tym przypadku jest to coś, co sprawi, że każda zmiana wywoła rekompilację naszego programu. Dodatkowo - jak połączyć takie narzędzie z kolejnym, czyli z Dockerem?
---

Za każdym razem kiedy próbuję postawić nowy projekt korzystając z języka Go, napotykam problem na konfigurację Dockera. Go wewnętrznie korzysta ze zmiennej środowiskowej `$GOPATH`, która jest ścieżką do całego ekosystemu Go na maszynie deweloperskiej. Do tego kod jest kompilowany do pliku binarnego, a sam język natywnie nie wspiera _hot reloada_. Możliwe jednak jest spięcie Go z Dockerem i uzyskanie hot reloada, nie jest to jednak tak proste jak w przypadku Node’a.

Ekosystem Dockera nie posiada jednego, wspólnego menadżera zależności jak to jest w przypadku innych środowisk. Tutaj możemy skorzystać np. z _Glide’a_ lub _depa_, który okazał się eksperymentem. Wraz z wersją 1.11 Go eksperymentalnie wprowadza _moduły_, które mają rozwiązać problem zależności oraz zmiennych środowiskowych. Zanim jednak moduły pojawią się w wersji produkcyjnej musimy korzystać ze starszych rozwiązań.

## Rezygnujemy z modułów

Pierwsza przykładowa konfiguracja Dockera dla Go będzie pokazana na prostym projekcie, który będzie budowany klasycznie (dla mnie). Folder z aplikacją umiejscowimy w naszej zmiennej środowiskowej `$GOPATH`, by kompilator mógł znaleźć zależności oraz by uzyskać wsparcie edytora/IDE. Do tego uznajmy, że nasz projekt będzie składał się z _podpaczek (subpackages)_, do których musimy podać pełną ścieżkę by móc je zaimportować.

Przykładowo: tworząc paczkę `food-tracker`, umieszczamy ją w ścieżce `gitlab.com/grzgajda/foodtracker`, gdzie całość reprezentuje adres URL do repozytorium, gdzie dana paczka będzie się znajdować.

Zastosowanie poprawnej ścieżki pozwoli na skompilowanie naszej paczki lokalnie - bez Dockera oraz podczas CI, gdzie będzie sprawdzana ścieżka do naszej paczki.

```yaml
version: '3'
services:
  our_service:
    build:
      context: '.'
    volumes:
      - '.:/go/src/gitlab.com/grzgajda/food-tracker'
    working_dir: '/go/src/gitlab.com/grzgajda/food-tracker'
    command: 'realize start'
    ports:
      - '8081:3000'
```

Powyższa konfiguracja pozwala na uruchomienie kontenera `our_service`, który będzie zawierał zbudowany przez nas obraz Dockera z podmontowanymi zasobami w odpowiednich ścieżkach. Jak widać, ścieżki do repozytorium muszą również zostać zachowane w naszej konfiguracji Dockerowej. Komenda [`realize start`](https://github.com/oxequa/realize) proces, który zadba o to by nasz kod był rekompilowany przy każdej zmianie.

```docker
FROM golang:alpine

# install realize
RUN apk --no-cache add --virtual build-dependencies git
RUN apk --no-cache add gcc musl-dev
RUN go get github.com/tockins/realize
```

Korzystając z takiego kodu dla pliku `Dockerfile` oraz `docker-compose.yml` jesteśmy w stanie w całości skonfigurować nasze środowisko deweloperskie, gdzie możemy pozwolić sobie na uruchomienie naszej usługi na dowolnej wersji Go oraz nie kompilować ręcznie całego kodu przy każdej a zmianie.

Polecam sprawdzić mój przykładowy projekt, _Split Into Pairs_, który posiada skonfigurowanego Dockera dla środowiska deweloperskiego oraz jest w pełni skonfigurowane CD pod Gitlab CI: [Split Into Pairs · GitLab](https://gitlab.com/split-into-pairs).

## Obraz na produkcję

Prodykcyjna wersja obrazu z aplikacją dla Go musi być lekka oraz zawierać jak najmniej zależności. W tym przypadku z ratunkiem przychodzą dla nas [multi-stage builds ](https://docs.docker.com/develop/develop-images/multistage-build/). Pozwalają nam one na skorzystanie z dwóch różnych obrazów w trakcie pojedynczego budowania obrazu. Przykładowy plik `Dockerfile` zapożyczony z dokumentacji dla Dockera:

```docker
FROM golang:alpine
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html
COPY app.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/github.com/alexellis/href-counter/app .
CMD ["./app"]
```

Jak możemy zauważyć, pierwszy _build_ tworzy nam gotowy, skompilowany program w formie pliku binarnego o nazwie _app_. Po skończeniu kompilacji tak naprawdę nie potrzebujemy już obrazu _golang:alpine_, który sam w sobie zajmuje ok. 310 MB miejsca na dysku. Zamiast tego możemy skopiować naszą aplikację do czystego obrazu `alpine`, który waży ok. 4.4 MB. Ponad 300 MB różnicy!

## Łączenie z zewnętrznymi serwisami

Kiedy pracujemy na naszym lokalnym środowisku postawionym dzięki _docker-compose_, wszystkie nasze kontenery powstają w tym samym czasie. Taka sytuacja może stać się o tyle problematyczna, że serwisy typu _MySQL_, _ElasticSearch_ wymagają więcej czasu do utworzenia się niżeli nasza aplikacja. Oczywiście powstało wiele narzędzi by rozwiązać ten problem, jednak jak z każdym narzędziem przychodzą kolejne problemy.

Ja taką sytuację eliminuję jednak inaczej. Tworzę nową funkcję, która jest odpowiedzialna za utworzenie połączenia do takiego serwisu i wymuszam w niej, by w razie wyrzucenia błędu powtórzyła swoją akcję po odczekaniu jednej zasady. Całość działa jak systemowy _ping_, który oczekuje aż funkcja zwróci wartość poprawną.

Dodatkowo - by nie wpaść w nieskończoną rekurencję, ustalam ile prób może aplikacja wykonać nim się zawiesi. Przykładowa funkcja próbująca utworzyć połączenie do bazy:

```go
func connectWithRetry(retryTimes int) *sql.Connection {
	conn, err := sql.Connect("some config there")
	if err != nil && retryTimes < 5 {
		time.Sleep(1 * time.Second)
		return connectWithRetry(retryTimes + 1)
	}
	if err != nil && retryTimes >= 5 {
		panic("cannot connect")
	}
	return conn
}
```
