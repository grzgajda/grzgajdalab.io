---
path: '/devmint-a-mind-of-creativity'
date: '2018-11-19'
title: 'devMint - a mind of creativity'
contentType: 'post'
language: 'pl'
cover: './cover.png'
intro: |
  Witam wszystkich na osobistym blogu zatytułowanym devMint - a mind of creativity. Nazywam się Grzegorz Gajda i jestem Software Developerem od 5 lat. Programuję w takich technologiach jak PHP, Javascript oraz Go, głównie aplikacje webowe.
---

Na tym blogu chciałbym opisywać wszystkie swoje myśli na temat programowania, więc nie każdy wpis musi być _”prawdziwym artykułem”_, _”tekstem wartym pokazania dalej”_. Głębszą ideą jest rozwijanie swojej programistycznej (i około programistycznej) wiedzy przelewając swoje myśli na paper, analizując je i udostępniając je Wam, byście mogli spojrzeć na mój tok rozumowania, może go zrozumieć, może go poprawić.

Dodatkowo w najbliższym czasie planuję napisać _”case studies”_ moich aplikacji publicznie dostępnych na moich repozytoriach GitLab (któremu powinienem podziękować za hostowanie tej strony). Posiadam parę repozytoriów, które zacząłem lub skończyłem tworzyć rok temu i analiza kodu sprzed roku, napisanego przez tego samego programistę, może przynieść różnorakie doświadczenia.
