---
path: '/segregacja-interfejsow'
date: '2018-12-18'
title: 'Segregacja interfejsów'
contentType: 'post'
tags: ['golang', 'php']
language: 'pl'
cover: './cover.png'
intro: Interfejsy to abstrakcyjne typy, które nie posiadają danych, stanu a jedynie definicje metod publicznych. Pełnią niejako rolę łączników lub kontraktów między zależnościami, komponentami, modułami oraz pozwalają na ich łatwe zamienianie. O implementacji interfejsu mówimy wtedy, gdy klasa posiada zdefiniowane wszystkie metody wymagane przez interfejs.
---

Interfejsy są częścią paradygmatu programowania obiektowego, które pozwalają na oddzielenie implementacji od abstrakcji. Niekiedy można spotkać się z innymi terminami opisującymi ten typ tj. protokoły lub _abstract base classes_. Są one również częścią języka Go, który nie jest językiem OOP. Interfejsy pozwalają na hermetyzację obiektów, dając dostęp tylko do wymaganych metod.

Interfejsy jako abstrakcyjne definicje ułatwiają pracę programiście. Pozwalają na napisanie testów jednostkowych klasy bez potrzeby tworzenia implementacji dla zależności, wystarczy zdefiniowanie ich abstrakcji. Ponadto takie zależności można w dowolnym momencie wymienić na inne, które implementują dany interfejs. Tak prowadzony kod nie opiera się na implementacji rozwiązań, a na abstrakcyjnym rozwiązywaniu problemów.

```go
package main

struct User {
	id int
}

interface UserRepository {
	FetchAll() []User
}

struct SqlUserRepository {}
func (r SqlUserRepository) FetchAll() []User {
	var users []User
	// fetch from sql
	return users
}

struct RedisUserRepository {}
func (r RedisUserRepository) FetchAll() []User {
	var users []User
	// fetch from redis
	return users
}

func RenderUsers(UserRepository repository) {
	users := repository.FetchAll()
}

func main() {
	var r UserRepository
	if os.Getenv("APP_ENV") == "prod" {
		r = SqlUserRepository{}
	} else {
		r = RedisUserRepository{}
	}

	RenderUsers(r)
}
```

Przykładowy kod powyżej pokazuje najprostsze zastosowanie interfejsów w projekcie. Nasza funkcja `RenderUsers` potrzebuje dostać jako zależność repozytorium, które będzie implementować metodę `FetchAll() []User`. Jednakże nie wiemy jaka będzie implementacja takiego repozytorium, dlatego możemy pobierać użytkowników z bazy danych SQL, z Redisa bądź innych rozwiązań do przetrzymywania danych lub tworzyć dane dynamicznie przy każdym wywołaniu metody (np. do celu testów).

## Zasada segregacji interfejsów

Zasada stworzona przez _Roberta C. Martina_, symbolizująca literę **I** w regułach **SOLID**. Robert C. Martin tłumaczy, że każda funkcja, klasa lub metoda, która posiada zależności, a zależności są definiowane przez interfejs, to dany interfejs powinien posiadać wyłącznie metody używane przez tę metodę, funkcję lub klasę.

```go
package main

struct User {}

interface FetchUsers {
	FetchAll() []User
}
interface FetchUserByID {
	FetchUserByID(int id) (User, error)
}

struct SqlUserRepository {}
func (s SqlUserRepository) FetchUsers() []User {
	var users []User
	// fetch all users from sql
	return users
}
func (s SqlUserRepository) FetchUserByID(int id) (User, error) {
	var user User
	// fetch single user from sql
	return user, nil
}
```

Powyższy kod pokazuje definicję dwóch interfejsów, `FetchUsers` oraz `FetchUserByID` oraz implementację tych dwóch interfejsów w stukturze `SqlUserRepository`. Posiadając tak zdefiniowane typy możemy w prosty sposób skonstruować handlery, np. dla naszego API:

```go
func RenderUsers(s FetchUsers) func(req Request, res Response) {
	return func(req Request, res Response) {
		json.NewEncoder(res.Body).encode(s.FetchAll())
	}
}

func RenderSingleUser(s FetchUserByID) func(req Request, res Response) {
	return func(req Request, res Response) {
		user, _ := s.FetchUserByID(req.Attr("id"))
		json.NewEncoder(res.Body).Encode(user)
	}
}

func main() {
	repo := SqlUserRepository{}

	r := SomeRouter()
	r.GET("/users", RenderUsers(repo))
	r.GET("/users/:id", RenderSingleUser(repo))
}
```

Jak widzicie, obie metody korzystają z tej samej struktury (w Go nie ma obiektów), jednakże są zależne od innego interfejsu. Dzięki temu nasza funkcja `RenderSingleUser` nie wie o tym, że może pobrać wszystkich użytkowników.

Nie podążanie tą zasadą często jest widoczne przy wykorzystaniu wzorca _Repository_, kiedy przykładowy interfejs `UserRepositoryInterface` posiada wszystkie metody, które są potrzebne programiście do pobierania użytkownika z bazy danych. Bardzo często widzę takie tworzenie interfejsów w aplikacjach PHP korzystających z _Doctrine_’a.

```php
interface UserRepositoryInterface
{
    public function add(User $user): void;

    public function remove(User $user): void;

    /**
     * @return User[]
     */
    public function findAll(array $orderBy, int $maxResults): ResultCollectionInterface;

    public function findOneByUsername(string $username): User;

    public function findOneByEmail(string $email): User;

    public function findOneById(UserId $id): User;
}
```

## Miejsce na interfejsy

Pora na bardzo ważne pytanie - gdzie powinniśmy trzymać nasze interfejsy? Jeżeli nasze repozytorium trzymamy w namespacie _models/users/repository_, a nasz kontroler jest w _actions/users_ to nasze interfejsy powinny znajdować się tam, gdzie z nich korzystamy czy tam, gdzie je implementujemy?

Pracując z takimi językami jak **Go** lub **Typescript** napotykałem się na to, że interfejs dla zależności jest bardzo blisko metody/funkcji/klasy/struktury, co potrzebuje tego interfejsu. W innym miejscu natomiast jest implementacja tego interfejsu. Takie rozwiązanie prowadzi do duplikacji interfejsów, ale gwarantuje 100% przestrzeganie 4. zasady SOLID.

![Interfejsy w PHP](./php-structure.png)

Pracując z **PHP** zauważam jednak inny trend, gdzie interfejs jest blisko implementacji, a klasy zależne od interfejsu są poukładane w różnych miejscach w projekcie. Taka metoda ogranicza liczbę interfejsów w projekcie oraz pozwala na ich ponowne użycie, odwrotnie do sytuacji zaprezentowanej wyżej.

Uważam, że obie metody są skuteczne, jednakże coraz częściej pierwsze rozwiązanie (gdzie interfejs jest blisko klasy zależnej a daleko od implementacji) sprawia mi mniej problemów w pracy. Taka struktura pozwala mi na przestanie o myśleniu gdzie utworzyć nowy interfejs a skupiam się głównie na tym, co ten interfejs powinien posiadać w sobie. W ten sposób gwarantuję projektowi rozdzielenie implementacji od abstrakcji i każdy komponent w kodzie jest osobnym bytem.
