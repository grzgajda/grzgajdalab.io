---
path:        "/witamy-tryb-nocny"
date:        "2019-03-31"
title:       "Witamy tryb nocny - 🌙"
contentType: "draft"
tags:        [ ]
language:    "pl"
---

Wraz z wydaniem wersji 12.1 przeglądarki Safari oraz wersji 67 przeglądarki Firefox, mamy możliwość dostosowywania naszych stron internetowych do trybu nocnego.

![Na CanIUse możemy sprawdzić jakie przeglądarki wspierają tryb nocny](./caniuse.png)

Nowe zapytanie `prefers-color-scheme` posiada trzy wartości: `no-preference`, `light` oraz `dark`. Za pomocą tych informacji możemy dopasować style naszej strony pod preferencje użytkownika.

![Porównanie trybu dziennego z nocnym](./light-dark-devmint.png)

Najprostszy sposób na szybkie podmienianie wartości to oczywiście zastosowanie zmiennych, które są dostępne w CSSie i wspierane przez różne przeglądarki. Znajdziemy je w Chrome wersji 49 i wzwyż, Firefox w wersji 31 i wzwyż, Edge w wersji 16 i wzwyż, Safari w wersji 9.1 i wzwyż. Tworząc nasze style do strony korzystając ze zmiennych, stworzenie *trybu nocnego* dla naszej strony nie jest wyzwaniem. Wystarczy zamienić taki kod:

```css
:root {
  --color-light: #fff;
}

h1 {
  color: var(--color-light);
}
```

na taki kod:

```css
:root {
  --color-light: #fff;
}

@media (prefers-color-scheme: dark) {
  :root {
    --color-light: #000;
  }
}

h1 {
  color: var(--color-light);
}
```

Dzięki tej prostej sztuczce bardzo szybko możemy wymienić całą naszą paletę kolorów i dopasować stronę do preferencji użytkownika.