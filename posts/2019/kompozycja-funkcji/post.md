---
path: '/kompozycja-funkcji'
date: '2019-04-23'
title: 'Kompozycja funkcji'
contentType: 'post'
tags: ['javascript', 'typescript']
language: 'pl'
cover: './cover.png'
intro: |
  Jednym z paradygmatów programowania obiektowego jest dziedziczenie, które polega na rozszerzeniu klasy bazowej o nowe metody lub nadpisanie już istniejących. Inną metodą rozszerzania istniejących klas jest ich kompozycja.
---

**Kompozycja** w odróżnieniu od dziedziczenia nie rozszerza istniejącej klasy, a jedynie implementuje jej interfejs. Dodatkowo nie dodajemy oraz nie dopisujemy zależności do istniejącej klasy (rozszerzanie klasy dalej wymaga wspólnego konstruktora), ponad to obie klasy mogą być tworzone równolegle ponieważ do dobrego działania kontraktuje je interfejs.

```javascript
import { compose } from 'lodash/fp'

const subtractVat = vat => amount => amount / vat
const subtractPit = pit => amount => amount / pit

const calculateTax = compose(
  subtractVat(1.23),
  subtractPit(18)
)
const result = calculateTax(10000)
```

Kompozycja pozwala na _połączenie_ wielu różnych metod lub funkcji, które posiadają wspólny interfejs. Interfejsem użytym w powyższym kawałku kodu byłby:

```typescript
type Calculator = (amount: number) => number
```

A użycie interfejsu wygląda następująco:

```typescript
const subtractVat => (vat: number): Calculator => amount => amount / vat
const subtractPit = (pit: number): Calculator => amount => amount / pit
```

Uproszczając powyższy kod jeszcze bardziej możemy otrzymać:

```typescript
const subtract = (sub: number): Calculator => amount => amount / sub
const subtractVat = subtract(0.23)
const subtractPit = subtract(0.18)
```

## Banalny debugging

Ideą stojącą za kompozycją jest _budowanie_ większych procesów z mniejszych komponentów, które kolejno przetwarzają podany argument. Rozbicie na naprawdę małe komponenty wielu procesów pomaga zwiększyć czytelność oraz testowanie kodu. By pokazać prawdziwą różnicę między różnymi podejściami, daję przykłady.

Nasza funkcja będzie musiała przetransformować tablicę z jednej postaci do drugiej.

```typescript
export interface Route {
  routeName: string
  routePath: string
}

export interface PhraseDictionary {
  phrase: string
  routes: string[]
}

const inputData: Route[] = [
  { routeName: 'homepage', routePath: '/' },
  { routeName: 'products_index', routePath: '/products' },
  { routeName: 'products_show', routePath: '/products/{id}' },
  { routeName: 'shops_index', routePath: '/shops' },
  { routeName: 'shops_show', routePath: '/shops/{id}' },
  { routeName: 'shops_products', routePath: '/shops/{id}/products' },
]

const outputData: PhraseDictionary[] = [
  {
    phrase: 'products',
    routes: ['products_index', 'products_show', 'shops_products'],
  },
  { phrase: 'shops', routes: ['shops_index', 'shops_show', 'shops_products'] },
]

export { inputData, outputData }
```

### Iterowanie element po elemencie

```typescript
function forLoop(input: Route[]): PhraseDictionary[] {
  const results: { [key: string]: string[] } = {}

  input.forEach(route => {
    const parts = splitLoop(route.routePath)

    parts.forEach(part => {
      createEmptyArray(results, part)
      results[part].push(route.routeName)
    })
  })

  return Object.keys(results).map(phrase => ({
    phrase,
    routes: results[phrase],
  }))
}
```

### Wbudowane metody `Array.prototype`

```typescript
function arrayMethods(input: Route[]): PhraseDictionary[] {
  const a = input
    .map(route => ({
      ...route,
      parts: splitArray(route.routePath),
    }))
    .reduce(
      (a, b) => {
        b.parts.forEach(part => {
          if (false === part in a) {
            a[part] = []
          }

          a[part].push(b.routeName)
        })

        return a
      },
      {} as { [key: string]: string[] }
    )

  return Object.keys(a).map(phrase => ({
    phrase,
    routes: a[phrase],
  }))
}
```

### Programowanie funkcyjne

```typescript
import * as R from 'ramda'

const addParts = (route: Route): IRouteWithPartsCollection => ({
  ...route,
  parts: splitCompose(route.routePath),
})

const groupToRoutes = (route: { p: string; r: string }) => route.p

const translateRoutes = R.pipe(
  R.map(addParts),
  R.chain(a => R.map(p => ({ p: p, r: a.routeName }), a.parts)),
  R.groupBy(groupToRoutes),
  R.toPairs,
  R.map(a => ({ phrase: a[0], routes: a[1].map(a => a.r) }))
)
```

## Ograniczona wydajność

Niestety korzystanie z kompozycji funkcji lub metod niesie ze sobą koszt. Źle skonstruowana kompozycja potrafi kilkukrotnie wykonać iterację po tej samej tablicy zwiększajac czas wykonywania aplikacji. Łatwo jednak zoptymalizować taki proces, nazewnictwo funkcji powinno szybko wskazać która funkcja ma większe obciążenie dla systemu. Również istnieją metody na „złączenie” wykonywanych funkcji w określonych przypadkach, co ma zapobiegać nadmiernej iteracji.

## Podsumowanie

Ciężko jest mi napisać więcej o kompozycji, na początku planowałem więcej wdrożyć się w ten temat, ale nagle wyskoczyły mi _generyki_ z TypeScripta, które nie pozwalają na wygodne korzystanie z kompozycji. Na potrzeby tego tekstu, stworzyłem również repozytorium zawierające dokładny kod i testy wykorzystany na potrzeby tego tekstu: [grzgajda/javascript-function-composition](https://gitlab.com/grzgajda/javascript-function-composition)
