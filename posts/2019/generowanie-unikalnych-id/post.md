---
path: '/generowanie-unikalnych-id'
date: '2019-02-01'
title: 'Generowanie unikalnych ID'
contentType: 'post'
language: 'pl'
cover: './cover.png'
intro: |
  Każda aplikacja, czy to jest webowa, desktopowa czy serwerowa, pracuje nad pewnego rodzajem danych, które następnie umieszcza do zewnętrznej bazy danych. By jednak wrócić do pracy na tych danych, aplikacja musi je jakoś rozpoznawać. Zazwyczaj służy do tego atrybut ID, czyli identifier.
---

Taki identyfikator może posiadać różne postacie, jednak zawsze musi to być wartość unikalna. W aplikacji monolitycznej może to być zwykły numer zawsze powiększany o 1 w stosunku do poprzednika. Jak wygląda jednak sytuacja gdy mamy dwie i więcej aplikacji, które tworzą identyfikator?

## AutoIncrement i baza RDBMS

Bardzo często wykorzystywanym generatorem unikatowych ID są bazy danych SQL oraz ich klucze `AUTO INCREMENT`, które automatycznie dołączają numerek wyżej od poprzedniego obiektu. Ograniczeniem takiego numeru jest jedynie wielkość kolumny.

Taka metoda jest bardzo często wykorzystywana przy aplikacjach małych, monolitycznych, ściśle przypisanych do konkretnej bazy danych. Rozmiar kolumny pozwala na zapisanie wielu obiektów, a sama numeryczna sekwencja pozwala na bardzo przyjemne sortowanie.

Przy takim rozwiązaniu problematycznym jednak jest operowanie na ID w momencie tworzenia obiektu, jeszcze przed utworzeniem rekordu w bazie danych. Możemy wcześniej pobrać ostatnio utworzony ID, jednakże w tym samym czasie inne żądanie lub inny proces może wykonać dokładnie tę samą operację. W takim przypadku zapis do bazy danych się nie powiedzie.

## UUID

**UUID** czyli _uniwersalny, unikalny identyfikator_ złożony ze 128 bitów, został szczegółowo opisany jako [RFC 4122](https://tools.ietf.org/html/rfc4122#section-4.1.1). Stosowanie UUID nie wymaga centralnej jednostki do sprawdzania unikalności, jest wspierany przez najróżniejsze bazy danych, składa się z czasu utworzenia więc na pewien sposób można po nim sortować.

UUID posiada 5 różnych wersji i każda z nich posiada ten sam format wyglądający jak `6beafa7b-ea2b-49f5-96eb-eb60730c74f8`. UUID w wersji 4 jako jedyny jest losowo generowany i nie wymaga żadnych danych wejściowych.

![Struktura UUID](./uuid.png)

UUIDv4 gwarantuje niską kolizyjność, ID jest tworzony z wykorzystaniem obecnego czasu na maszynie. Te dwie zalety sprawiają, że UUID jest dobrym rozwiązaniem, jednakże ciężkim. Jeden taki identyfikator zajmuje aż 128 bitów. 1 kilobajt danych to tylko 16 identyfikatorów. Korzystając z kolumny `BIGINT`, bylibyśmy w stanie wygenerować 16 razy więcej identyfikatorów.

## Snowflake

**Snowflake** to wynalazek Twittera, który służy do generowania tysięcy unikalnych ID dla różnych encji. Identyfikator wygenerowany przez Snowflake zajmuje 64 bitów, składa się z trzech części: obecny czas maszyny, identyfikator maszyny, sekwencja numeryczna oraz 1 wolny bit, zarezerwowany na przyszłość.

![Struktura Snowflake](./snowflake.png)

Snowflake w odróżnieniu od innych kluczy wymaga postawionej osobnej usługi, z którą wszystkie inne usługi będą musiały się komunikować by otrzymać nowy identyfikator. Dodatkowo możemy postawić dodatkowe serwisy czyli zeskalować naszą usługę na większą liczbę maszyn (unikalność gwarantuje nam identyfikator maszyny).
