---
path: '/3-2-1-go'
date: '2019-06-17'
title: '3. 2. 1. Go!'
contentType: 'post'
tags: ['golang']
language: 'pl'
cover: './cover.png'
intro: |
  15 marca 2018 wziąłem udział w 6. edycji "BarCoders", na której opowiadałem o podstawach współbieżności, asynchroniczności oraz programowaniu równoległego. Całość przykładów pokazywałem na podstawie języka Go.
---

Prezentacja podzielona była na 2 etapy. Najpierw zacząłem opowiadać o historii języka Go, jego mozliwościach oraz zastosowaniach by płynnie przejść na pokazanie róznych metodologii programowania.

<iframe 
  src="https://www.icloud.com/keynote/090POn6LvE-L4vpK7Hbp9ZVeg?embed=true" 
  width="670" 
  height="546" 
  frameborder="0" 
  allowfullscreen="1" 
  referrer="no-referrer"
></iframe>
