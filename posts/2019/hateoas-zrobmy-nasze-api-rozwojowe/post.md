---
path: '/hateoas-zrobmy-nasze-api-rozwojowe'
date: '2019-03-23'
title: 'HATEOAS - zróbmy nasze API rozwojowe'
contentType: 'post'
tags: ['restful api']
language: 'pl'
cover: './cover.png'
intro: |
  API (Application Programming Interface) jest kontraktem zawartym między klientem, a serwerem, gdzie klientem może być każdy - aplikacja webowa, mobilna czy proces działający w tle systemu operacyjnego. Kontrakt ten pozwala na komunikowanie się między aplikacjami (lub komponentami w aplikacji) według zdefiniowanej składni pytań i odpowiedzi.
---

W tym tekście używając zwrotu _API_ będę mówił o komunikacji między aplikacjami po protokole _HTTP_.

## Jak funkcjonują API?

Wchodząc pod adres _https://devmint.pl/_, przeglądarka wysyła żądanie HTTP z odpowiednimi nagłówkami oczekując na odpowiedź. I żądanie, i odpowiedź, prócz nagłówków może posiadać również zawartość. W przypadku tego adresu odpowiedzią będzie kod HTML potrzebny do wyświetlenia strony. Kod HTML jest swoistym kontraktem pomiędzy serwerem (dostawcą treści) a przeglądarką. Protokół HTTP nie definiuje jednak rodzaju odpowiedzi, wchodząc więc pod adres _https://devmint.pl/sitemap_ dostaniemy plik XML, a pod adresem _https://devmint.pl/robots.txt_ dostaniemy plik tekstowy.

Ja bym chciał dzisiaj porozmawiać o przesyłaniu wiadomości o formacie **JSON**, który obecnie jest najpopularniejszym językiem w przetwarzaniu informacji w Internecie, ze względu na swoją prostotę i czytelność.

Przypuśćmy, że znaleźliśmy serwis udostępniający listę książek w księgarniach w danym mieście. Pod jakimkolwiek adresem URL znajdujemy dokumentację, np. popularny _Swagger_, który opisze nam każdy zasób, w tym przypadku _książki_, może _miasta_. Wymyślmy sobie zadanie - _chcemy znaleźć nazwy wszystkich miast, w których możemy kupić konkretną książkę_.

Wiemy, że endpoint `GET /books` zwraca nam listę wszystkich książek, a operując parametrem `page` możemy przemieszczać się po liście wprzód i w tył. Znajdujemy naszą książkę, przykładowy zasób mógłby wyglądać tak:

```json
{
  "title": "Year 1984",
  "author": "George Orwell",
  "cities": [1, 4, 19]
}
```

Znamy tytuł książki oraz ID wszystkich miast, w których ta książka się znajduje. Teraz wystarczy, że pobierzemy dane na temat tych miast. Czytamy w dokumentacji, że endpoint to `GET /books/1`, `GET /books/4` oraz `GET /books/9`. Proste, prawda?

## Zawiązaliśmy kontrakt

W ten sposób zawiązaliśmy kontrakt pomiędzy naszą aplikacją a zewnętrznym API. Oznacza to, że wymusiliśmy na dostawcy API niezmienność endpointów, które dostarczają nam dane oraz formatu danych, których on dostarcza. Jakakolwiek zmiana sprawia, że wszyscy konsumenci API nie są już w stanie korzystać z tych danych i muszą zaktualizować kontrakt w swoich aplikacjach klienckich. Czy to znaczy, że raz stworzone API nigdy nie może zostać zmienione?

Oczywiście, że nie. Programiści wymyślili sobie różne mechanizmy pozwalające im dalej rozwijać API. Jednym z nich jest wersjonowanie, a to przez specjalny endpoint (np. `GET /v1/books`) lub dodatkowy nagłówek (np. `Version: 1`). Oba sposoby są społecznie akceptowalne i nie robią większej każdy klientom, osoba dopiero co korzystająca z API skorzysta z najnowszej wersji, a osoby podłączone będą korzystały odpowiednio ze starszych wersji.

Problemem staje się jednak utrzymanie samego API, zamiast jednej metody do wyświetlania książek mamy ich tyle, ile wersji posiada API. Każda z tych metod musi być utrzymywana. Dodatkowo wszystkie cechy i funkcjonalności danego endpointa muszą zostać zachowane, tj. domyślny kierunek sortowania, nazwy kluczy, po których są filtrowane dane itd. Z biegiem czasu proste API może się przerobić w dużą aplikację z powtarzalnym kodem.

## HATEOAS

Poruszając się po aplikacjach webowych, użytkownik nie zna dokładnego adresu, na który chce się dostać. Jeżeli szukamy butów do kupienia konkretnej wchodzimy na sklep konkretnej firmy, a następnie nawigujesz za pomocą menu. Dlaczego w przypadku API ma być inaczej? Z tego powodu wymyślono termin **Hypermedia As The Engine Of Application State**, w skrócie **HATEOAS**.

Mając nasze API z księgarniami i nie posiadając dokumentacji, wchodzimy pod główny endpoint czyli `GET /` i dostajemy taką odpowiedź:

```json
{
  "_links": [
    { "rel": "books", "href": "/books" },
    { "rel": "cities", "href": "/cities" }
  ]
}
```

Prócz pustej odpowiedzi dostajemy również adresy URL, pod którymi znajdujemy kolejne treści określone słowami kluczowymi. Szukając książek, wiemy, że potrzebujemy zajrzeć pod endpoint `GET /books`, mówi nam o tym słowo kluczowe _”books”_. Wchodzimy więc na ten URL i dostajemy:

```json
{
  "data": [
    {
      "title": "Year 1984",
      "author": "George Orwell",
      "cities": [1, 4, 19],
      "_links": [
        { "rel": "city", "href": "/cities/1" },
        { "rel": "city", "href": "/cities/4" },
        { "rel": "city", "href": "/cities/19" }
      ]
    }
  ],
  "_links": []
}
```

Tym razem sekcja adresów URL jest pusta, ale każdy zasób dostępny pod kluczem `data` posiada własne adresy URL. Dzięki temu wiemy, że chcąc pobrać dane nt. pojedynczego miasta, musimy dostać się pod endpoint `GET /cities/1`. I to bez zaglądania do dokumentacji.

Dzięki prostemu zabiegowi, jakim jest dodanie adresów URL dostępnych dla użytkownika, nie musimy (choć dalej możemy) tworzyć dokumentacji dla konsumentów, odpowiedzi z API same tłumaczą, co użytkownik może zrobić dalej.

## Podsumowanie

W tym wpisie wytłumaczyłem czym jest _API_, jedną z zalet implementacji _HATEOAS_ w naszym API. Nie jest to jednak koniec zalet, po więcej szczegółów zapraszam pod hashtag [#restful-api](/tags/restful-api).
