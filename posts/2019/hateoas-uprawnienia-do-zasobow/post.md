---
path: '/hateoas-uprawnienia-do-zasobow'
date: '2019-03-25'
title: 'HATEOAS - uprawnienia do zasobów'
contentType: 'post'
tags: ['restful api']
language: 'pl'
cover: './cover.png'
intro: |
  W poprzednim wpisie zaprezentowałem w jaki sposób poinformować użytkownika pod jakim adresem URL znajduje się konkretny zasób. Dzisiaj chciałbym trochę rozszerzyć ten wątek o uprawnienia. Rzadko kiedy API może udostępniać wszystkie swoje zasoby każdemu użytkownikowi, z tego powodu część z nich się chowa i udostępnia upoważnionym użytkownikom.
---

Powracając do wpisu [HATEOAS - zróbmy nasze API rozwojowe](/hateoas-zrobmy-nasze-api-rozwojowe), weźmy znowu nasze API pełne miast i książek. Teraz jednak nie jesteśmy klientami poszukującymi książki w danym mieście, a właścicielem księgarni, który zakupił nową książkę. Standardowo, otwieramy dokumentację API i znajdujemy endpoint `POST /books`, który wymaga od nas odpowiednich pól. Wysyłamy nasze żądanie i otrzymujemy…

## 403 Forbidden

Obowiązkiem właściciela strony jest nieudostępnianie poufnych, niebezpiecznych treści drugiej osobie. Takie działanie składa się z wielu elementów jak _nadawanie uprawnień użytkownikom odpowiednich ról i przywilejów do ich kont_, _nieudostępnianie adresów URL, które zawierają nieodpowiednie dane_. W tym momencie, programista posiadając takie opcje może wykonać jeden z najczęstszych ruchów: stworzyć nowe API tylko dla uprawnionych użytkowników, stworzyć nową dokumentację i udostępnić to wyłącznie użytkownikom z odpowiednią rolą. Jak to wygląda w praktyce?

Administrator księgarni chce dodać nowy zasób do bazy danych, sprawdza dokumentację _tylko dla administratorów księgarni_ i widzi, że musi wykonać żądanie pod endpoint `POST /admin/books` ze specjalnym nagłówkiem `Authorization: Bearer magic_token`. Nie ma jednak pewności czy posiada dostęp do tego zasobu dlatego eksperymentalnie wykonuje pierwsze żądanie.

## 201 Created

W poprzednim wpisie przedstawiałem jak może wyglądać odpowiedź w postaci JSON z endpointa `GET /`. Weźmy tę odpowiedź i dodajmy do niej nowy link.

```json
{
  "_links": [
    { "rel": "books", "href": "/books", "method": "GET" },
    { "rel": "cities", "href": "/cities", "method": "GET" },
    { "rel": "books.create", "href": "/books", "method": "POST" }
  ]
}
```

W prosty sposób poinformowaliśmy konsumenta API pod jakim adresem URL oraz jakiej metody HTTP musi użyć by utworzyć nowy zasób. Czym to się jednak różni od poprzedniej metody? Zwracając odpowiedź serwer już wtedy wie czy dana osoba posiada uprawnienia czy nie - nie powinniśmy więc wymuszać kolejnej czynności na użytkowniku tylko od razu poinformować go o możliwościach.

Identycznie wygląda sytuacja gdy korzystamy z normalnych stron WWW. Wchodząc na jakikolwiek sklep nie posiadamy dostępu do formularza dodawania produktu, musimy najpierw się zalogować, a system na podstawie naszych uprawnień pokaże nam czy możemy wykonać taką czynność czy nie. Takie oddziaływanie z konsumentem API jest o tyle fajne, że o wszystkich uprawnieniach, możliwościach nie myśli konsument tylko API. Widzę URL - mogę to zrobić.

## Paginacja

Przeglądając klasyczną aplikację e-commerce nigdy nie dostajemy wszystkich produktów na jednej podstronie. Jest to nieekonomiczne, i dla użytkownika, i dla sklepu. Przeglądając API należące do e-commerce również nie powinno dojść do takiej sytuacji. Ale dlaczego odpowiedzi z API najczęściej wyglądają tak:

```json
{
  "data": [],
  "totalItems": 35642
}
```

Taka odpowiedź wymusza na użytkowniku ręczne przeliczanie czy istnieje następna strona. API zwraca 5000 elementów, elementów mam 35642, ja jestem na stronie drugiej więc wyświetliłem już 10000 elementów, ok, mogę wejść na kolejną stronę. Niepotrzebna praca dla każdego jednego konsumenta API kiedy tak naprawdę tę czynność może wykonywać API - raz.

```json
{
  "data": [],
  "_links": [
    { "rel": "self", "href": "/products/2" },
    { "rel": "next", "href": "/products/3" },
    { "rel": "prev", "href": "/products" }
  ]
}
```

Ostatnio implementowałem API, które zwracało odpowiedź wg. pierwszego wzorca, tj. liczbę wszystkich elementów dostępnych w zasobie. Ja musiałem za jednym razem pobrać je wszystkie jednak API ograniczało ilość elementów wyświetlanych jednocześnie. Z tego powodu robiłem _bzdurne_ pierwsze zapytanie do API by dostać tylko tę jedną informację - `totalItems` i na jej podstawie wyliczyć ile zapytań będę musiał zrobić. (P.S. tak wiem, nie musiałem robić dodatkowego zapytania).

## Podsumowanie

Jest to drugi artykuł opowiadający o HATEOAS, tym razem mam nadzieję, że w ciekawy sposób pokazałem jak manipulując nawigacją możemy wskazać odpowiednim użytkownikom odpowiednie zasoby, a innym użytkownikom te zasoby schować bez tworzenia dodatkowych dokumentacji.
