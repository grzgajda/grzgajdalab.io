---
path: '/styled-is-oraz-styled-flex-w-wersji-typescript'
date: '2019-01-08'
title: 'styled-is oraz styled-flex - w wersji TypeScript'
contentType: 'post'
tags: ['typescript', 'styled-components', 'react']
language: 'pl'
cover: './cover.png'
intro: |
  Bardzo ciekawym trendem na przestrzeni ostatnich dwóch lat jest zrezygnowanie z plików css lub scss na rzecz metody CSS-in-JS, która polega na bezpośrednim pisaniu styli w „komponentach”, które możemy następnie wykorzystać w naszym projekcie. By jednak lepiej zrozumieć CSS-in-JS należy zrozumieć koncepcję komponentów.
---

**Komponenty** to najmniejsze elementy w naszej aplikacji, które potrafią coś ze sobą „reprezentować”. Takim komponentem może być `<button />`, `<a />` lub całkowicie nowy twór typu `<Avatar />`. Większość dzisiejszych frameworków Javascript dla przeglądarek opiera się właśnie o komponenty, np. _React_, _Angular_, _Vue_ czy _Polymer_. Dzisiaj będziemy mówić o bibliotece [styled-components](https://www.styled-components.com), która została stworzona specjalnie dla Reacta.

```jsx
import styled from 'styled-components'

const Avatar = styled.div`
  display: block;
  background: url('some-img.png') no-repeat;
`
```

Zastosowanie _styled-components_ pozwala w bardzo szybki sposób tworzyć nowe komponenty, które powinny być „głupie” (czyli nie posiadać żadnej logiki), a jedynie dostarczać wartości wizualne. Nasz komponent `<Avatar />` w tym momencie jest po prostu zwykłym `<div />`, który posiada dwa style: `display` oraz `background`. Wygodne, prawda? Jednak by lepiej zrozumieć przewagę _styled-components_, muszę pokazać lepszy przykład.

```jsx
import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'

const Button = styled.button`
  display: block;
  border: none;

  ${props => (props.outline ? `border: 1px solid green;` : '')};
  ${props => (props.primary ? `background: green` : '')};
  ${props => (props.secondary ? `background: gray` : '')}
`

ReactDOM.render(
  <Button primary>To jest przycisk koloru zielonego</Button>,
  document.getElementById('root')
)
```

Komponenty utworzone przez `styled` są wciąż normalnymi komponentami dla Reacta więc możemy wykorzystać takie elementy jak _propsy_. Jednakże korzystanie z propsów nie musi być najwygodniejsze do tworzenia warunków, pokazuje ten wycinek kodu z wyżej. Społeczeństwo jest jednak szczodre i znalazłem bardzo ciekawe rozwiązanie - `styled-is`. Paczka nie posiadała jednak wsparcia dla _TypeScript_ więc postanowiłem napisać całość samemu.

## [typescript-styled-is](https://www.npmjs.com/package/typescript-styled-is)

Celem tej paczki było stworzenie prostego systemu dla `if` w kodzie by tworzyć czytelniejszy kod, a do tego stworzenie wsparcia dla TypeScript (tzw. _typings_). Dzięki tej bibliotece nie musimy już korzystać z operatora warunkowego (_ternary operator_), do tego zachowujemy czytelność jeżeli nasz warunek dodaje więcej niż jedną linię styli.

```typescript
import is from 'typescript-styled-is'
import styled from 'styled-components'

const Box = styled.div<IBoxProps>`
  display: block;

  ${is('inline')`
    display: inline;
  `}
`
interface IBoxProps {
  inline?: boolean
}

/*
 * display: block
 */
<Box />

/*
 * display: block;
 * display: inline;
 */
<Box inline={true} />
```

Tak ułatwiona kompozycja warunków pozwala na pisanie komponentów do wielu zastosowań jednocześnie jak np. własny system grid lub 1 przycisk z wieloma kolorami. Ja potrzebowałem tej biblioteki do stworzenie własnego, prywatnego systemu _Flex_ opartego o styled-components.

## [typescript-styled-flex](https://www.npmjs.com/package/typescript-styled-flex)

Wzorowana na paczce [styled-flex-component](https://www.npmjs.com/package/styled-flex-component), rozszerza ją o wsparcie dla _Typescript_. Paczka pozwala na tworzenie szkieletu szablonów korzystając z jednego z dwóch komponentów, `Flex` oraz `FlexItem`.

```jsx
<Flex column>
  <Flex alignCenter justifyCenter>
    Header
  </Flex>
  <Flex row>
    <Flex column>Sidebar</Flex>
    <Flex>Content</Flex>
  </Flex>
</Flex>
```

Powyższy przykład pokazuje, że jeden komponent jest w stanie odpowiadać za cały szkielet i tak naprawdę potrzeba wyłącznie dodania niewielu styli (który może wstrzyknąć przez atrybut `style` lub po prostu stworzyć nowy komponent metodą `styled(Flex)`).

Komponent `<Flex />` posiada skonfigurowane wszystkie atrybuty do konfigurowania jak `<Flex alignCenter />` lub `<Flex wrap />`. Całość jest czytelnia opisana w repozytorium w pliku [src/Flex.ts](https://gitlab.com/grzgajda/typescript-styled-flex/blob/master/src/Flex.ts). Dodatkowo można zauważyć jak zachowana jest czytelność komponentu mimo użycia tak wielu warunków.

_CSS Flex_ jest naprawdę elastyczny jeżeli chodzi o wykorzystywanie go, pozwala na zrobienie wielu ciekawych rzeczy w prosty sposób. Ta paczka miała ułatwić korzystanie z flex’a wszystkim użytkownikom Reacta i Typescripta.

## Podsumowanie i przyszłość

Obie paczki - `typescript-styled-is` oraz `typescript-styled-flex` to moja pierwsza przygoda z open source na NPMie dlatego też wartości, które wnoszą do projektu są nie wielkie - ale wciąż są. Z wiedzą jak należy budować paczki (`npm publish`, CI do automatycznego budowania) będę mógł w najbliższej przyszłości stworzyć coś innego dla społeczności open source.

A co do `typescript-styled-is`… tam brakuje mi jednej funkcjonalności - łączenia warunków w jedno. Obecnie, gdy nasz komponent spełnia dany warunek, style są renderowane i możliwe, że nadpisywane przez kolejne warunki. Można by było to wyeliminować wprowadzając np. taką składnię:

```typescript
import is, { isNot } from 'typescript-styled-is'
import styled from 'styled-components'

const Box = styled.div<IBoxProps>`
  display: block;
 
  ${is('inline', isNot('flex'))`
    display: inline;
  `}
  ${is('flex', isNot('inline'))`
    display: flex;
  `}
  ${is('flex', 'inline')`
    display: inline-flex;
  `}
`
interface IBoxProps {
  inline?: boolean
  flex?: boolean
}
```
