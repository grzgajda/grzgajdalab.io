---
path: '/generatory-nie-listy'
date: '2019-03-03'
title: 'Generatory, nie listy'
contentType: 'post'
tags: ['php']
language: 'pl'
cover: './cover.png'
intro: |
  Niemalże w każdej aplikacji jednym z obowiązków programisty jest przyniesienie pewnej listy danych z jednego miejsca do drugiego. To naturalny proces, w końcu nie zawsze jesteśmy w stanie operować na tych danych w jednym miejscu. W tym celu korzystamy z tablic (array, slice).
---

Na etapie projektowania aplikacji rzadko kiedy biorę pod uwagę przyszłe oczekiwania klienta oraz rozrost aplikacji. Zazwyczaj pracuję na tablicach kiedy mam przygotować jakieś dane i coś z nimi zrobić. Tym samym nie raz zdarzyło mi się źle zaprojektować aplikację na samym początku ponieważ nie wytrzymywała procesowania danych.

Bardzo prostym rozwiązaniem na procesowanie, przetwarzanie oraz przenoszenie danych są generatory. Ja je rozumiem jako _dynamiczne tworzenie elementów tablicy przy każdym wykonaniu funkcji_. W języku PHP generatory są zwykłymi iteratorami, gdzie każde kolejne przywołanie następnego elementu (metoda `next()`) najpierw tworzy (lub wykonuje pewne procesy) ten element.

```php
<?php declare(strict_types=1);

function generateRandomNumbers(int $max): iterable
{
    $randomNumbers = [];
    for ($i = 0; $i < $max; $i++) {
        $randomNumbers[] = random_int(1, $max);
    }

    return $randomNumbers;
}

function asyncGenerateRandomNumbers(int $max): iterable
{
    for ($i = 0; $i < $max; $i++) {
        yield random_int(1, $max);
    }
}
```

Generatory najlepiej się odnajdują w sytuacjach gdy musimy przetworzyć dane z zewnetrznego źródła. Gdy nasza aplikacja wymaga od nas, przykładowo, wyciagnięcia danych z pliku _csv_ i umieszczenia natychmiastowo tych danych w jakiejkolwiek bazie to czy potrzebujemy pierw gromadzić te dane w pamięci aplikacji? Co w przypadku gdy plik zajmuje więcej pamięci niż nasza maszyna udostępnia?

## Kolekcjonowanie transakcji z serwisów afiliacyjnych

W 2016 roku dostałem zlecenie na mikro aplikację, prosty panel afiliacyjny zbierający dane z paru (wtedy chyba z 20) zewnętrznych API. Dostarczono mi specyfikację tych API, przedstawiono podstawowe wymagania tj. pobieramy wszystkie transakcje i wszystkie kliknięcia by następnie je wyświetlić sumarycznie. Aplikacja ma codziennie pobierać nowe dane z ostatniego tygodnia.

Wtedy jeszcze jako niezbyt doświadczony programista w rozmowach z klientami, nie znający realiów „ostatniego tygodnia” oraz „paru zewnętrznych API” zabrałem się za pisanie kodu. Pomysł był prosty: każda zewnętrzna usługa jest odwzorowana dwoma serwisami w aplikacji, pierwszy jest odpowiedzialny za pobieranie nowych danych, drugi za procesowanie. Dopiero jak pobrałem i uporządkowałem wszystkie dane, zwracałem je do _głównego procesu_ gdzie dochodziło do zapisu.

Wszystkie API z listy dostarczonej przez klienta pozwalały na pobieranie danych dzień po dniu, zwiększajac w ten sposób liczbę żądań a ograniczając ilość przesyłanych bajtów. To był mój pomysł na odciążenie pamięci. Jeden dzień posiadał maksymalnie do tysiąca transakcji więc bezproblemowo aplikacja dawała sobie z tym radę.

Jakiś czas później klient poprosił o dodanie kolejnych serwisów. I tu pojawił się pierwszy problem, jeden z nich nie wspierał filtrowania danych dzień po dniu - po prostu ograniczał liczbę żądań do 10 na godzinę. A zmieniły się również zalecenia klienta - raz dziennie aplikacja ma pobierać historię wszystkich transakcji i kliknięć do dwóch lat wstecz.

## Ignorancja szkodzi

Projekt opisany przeze mnie wyżej nie był _agile_, był stworzony pod sztywną specyfikę paru API. Wystarczył jeden kolejny serwis by zweryfikować model działania pobierania danych, a rozwiązaniem tego problemu były generatory.

Nie użyłem generatorów w tej aplikacji ponieważ nie znałem ich zastosowania, uważałem je za konstrukt który pozwala czytać linię po linii w pliku, nic więcej. Nie pomyślałem, że mogą być użyteczne do przetwarzania tysięcy rekordów, pozwalają na wstrzymywanie pracy w trakcie a tym samym na lepsze zarządzanie pamięcią aplikacji oraz jej czasem wykonywania.
Skutkiem takiego postępowania było przeniesienie aplikacji na osobny serwer z wystarczająca ilością pamięci oraz _ręczne czyszczenie pamięci_ po skończonej akcji. Obecnie, każdy serwis który wymaga od razu pobrania wszystkich danych ze swojego API jest problemem do rozwiązania. W pamięci trzymamy surowe oraz przetworzone dane, mimo że potrzebujemy wyłącznie przetworzonych. Nie mamy również możliwości strumieniowania odpowiedzi HTTP od klienta (nawet w przypadku tak banalnego strumieniowania plików CSV).

## Na przyszłość

Od kiedy zrozumiałem błąd w projektowaniu wyżej opisanej aplikacji, w 99% przypadkach pobierania danych z API staram się to robić partiami, _asynchronicznie_ by zostawić sobie furtkę na rozwój w przyszłości. Do tego praca na generatorach dodatkowo daje mi wyzwanie, kiedy nie mogę sprawdzić ile elementów mam do przetworzenia.
