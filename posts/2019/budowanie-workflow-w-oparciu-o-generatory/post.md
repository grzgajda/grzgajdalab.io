---
path: '/budowanie-workflow-w-oparciu-o-generatory'
date: '2019-05-07'
title: 'Budowanie workflow w oparciu o generatory'
contentType: 'post'
tags: ['javascript']
language: 'pl'
cover: './cover.png'
intro: |
  W jednym z poprzednich artykułów opisywałem pokrótce czym są generatory w językach oprogramowania oraz do czego służą. Dzisiaj chciałbym przedstawić ich kolejne zastosowanie, a będzie to budowanie workflow, czyli procesu zbudowanego z paru kroków.
---

By lepiej zobrazować sobie całość, postarajmy się przyjąć jakiś realny scenariusz, np. proces zakupowy w sklepie. By nie przesadzać z przykładem postaramy się zrobić prosty `workflow` który będzie wyglądał następująco:

<iframe src="//my.visme.co/embed/1jr4wpm9-untitled-project" height="650" width="800" style="border: 0px;" webkitAllowFullScreen mozallowfullscreen allowFullScreen>

Wiedząc już jak ma wyglądać cały proces, zabieramy się do kodowania.

## Najpierw testy

Zacznijmy od etapu **_”Każdy produkt kolejno jest sprawdzany czy znajduje się na magazynie”_**. Naszym założeniem jest, że każdy proces zwróci nam **_event_**odpowiadający pewnej akcji. Jeżeli wszystkie produkty znajdują się w magazynie to powinniśmy dostać obiekt odpowiadający mniej więcej takie strukturze:

```json
{
  "event": "ALL_PRODUCTS_IN_WAREHOUSE",
  "products": [...]
}
```

Jeżeli któryś z produktów nie znajduje się w magazynie, powinniśmy dostać coś takiego:

```json

{
  "event": "PURCHASED_PRODUCT_IS_NOT_AVAILABLE",
  "products": [...],
  "not_avaiable": [...]
}
```

Skoro mamy pewne założenia i wiemy czego możemy oczekiwać, kolejnym krokiem jest napisanie naszych testów do tych dwóch przypadków:

```javascript
describe('workflow of purchasing products', () => {
  const inWarehouse = ({ id }) => ![1, 2].includes(id)
  const processPurchase = workflow(inWarehouse)

  it('should check if all products are in the warehouse and failed when one of products is not there', () => {
    const products = createProducts(4)
    const firstReturn = processPurchase(products).next().value

    expect(firstReturn.event).toEqual('PURCHASED_PRODUCT_IS_NOT_AVAILABLE')
    expect(firstReturn.not_available).toEqual([{ id: 3 }, { id: 4 }])
  })

  it('should check if all products are in the warehouse and go to the next step if it is true', () => {
    const products = createProducts(2)
    const firstReturn = processPurchase(products).next().value
    expect(firstReturn.event).toEqual('ALL_PRODUCTS_IN_WAREHOUSE')
  })
})
```

Jak można zauważyć, nasz proces działa w oparciu o iterator dlatego w trakcie testowanie używamy metody _`Iterator.next()`_, która przenosi nas do kolejnego etapu i zwraca nam wartość (atrybut _`value`_). Takie zachowanie jest o tyle wygodne, że nie musimy przetwarzać naszego procesu w pętli a wywoływać każdy krok samodzielnie, w różnych momentach kodu bowiem iterator, jak każdy obiekt, można przekazywać dalej.

## Potem kod

Skoro mamy napisany pierwszy test, pora uzupełnić go o kod. Każda funkcjonalność procesu, jak sprawdzenie stanu magazynowego lub zapisanie wyniku do bazy danych powinno być wykonywane przez zewnętrzne serwisy, które należy wstrzyknąć.

```javascript
module.exports = function(inWarehouse) {
  return function*(products) {
    const notInWarehouse = products.filter(inWarehouse)
    if (notInWarehouse.length > 0) {
      yield {
        event: 'PURCHASED_PRODUCT_IS_NOT_AVAILABLE',
        products,
        not_available: notInWarehouse,
      }
    }

    yield { event: 'ALL_PRODUCTS_IN_WAREHOUSE', products }
  }
}
```

Nie możemy zapomnieć o tym, by przerywać cały proces w sytuacji, kiedy coś poszło nie tak. Zaraz po wyrzuceniu **_eventu_**z brakiem produktu na magazynie, dodajmy linijkę:

```javascript
return {
  event: 'PROCESSING_STOPED',
  cause: 'PURCHASED_PRODUCT_IS_NOT_AVAILABLE',
}
```

Musimy również dopisać kolejny test:

```javascript
it('should stop processing further if one of products is not in the warehouse', () => {
  const products = createProducts(4)
  const processor = processPurchase(products)
  const firstReturn = processor.next()
  const secondReturn = processor.next()

  expect(secondReturn.value.event).toEqual('PROCESSING_STOPED')
  expect(secondReturn.value.cause).toEqual(firstReturn.value.event)

  while (processor.next().value !== undefined) {
    expect(true).toBeFalsy()
  }
})
```

W tym teście bardzo fajnie można zauważyć przewagę iteratora. Dzięki wykorzystaniu pętli `while` wykonujemy wszystkie kolejne kroki, jednakże przy pierwszym znalezionym kroku nasza asercja się nie sprawdza. Teraz mamy pewność, że zawsze jak jakikolwiek z produktów nie znajduje się na magazynie, cały proces ulega zakończeniu zwracając nam o tym informację.

## Procesowanie zakupu

Skoro wiemy jak wygląda podstawowy test oraz kod takiego procesu, spróbujmy teraz napisać resztę. By jednak to zrobić, musimy sprecyzować parę rzeczy:

- Sumowanie cen wyłącznie jest informacją dla programisty, która wychodzi z całego procesu jako _event_ oraz jest przekazywana dalej do kolejnych procesów. Ten etap zawsze przechodzi dalej.

- Zapis do bazy danych wymaga dwóch informacji, listy produktów oraz sumę cen. Zapis jest również wykonywany przez zewnętrzny serwis więc nas interesuje wyłącznie informacja czy czynność się powiodła czy nie.

- Identyczna sytuacja jest w przypadku wysyłaniu maili, interesuje nas wyłącznie stan dokonany, nie implementacja.

Mając podstawowe wytyczne, możemy teraz przejść do pisania kolejnych testów. Do zliczania cen do naszego produktu musimy dodać nowy atrybut, nazwijmy go `price`.

```javascript
it('should sum price of all products', () => {
  const products = createProducts(2)
  const processor = processPurchase(products)

  const stepAllProductsInWarehouse = processor.next()
  const stepPriceOfAllProducts = processor.next()

  expect(stepPriceOfAllProducts.value.event).toEqual('SUM_PRICE')
  expect(stepPriceOfAllProducts.value.price).toEqual(30)
})
```

```javascript
it('should store products with price into database', () => {
  const products = createProducts(2)
  const processor = processPurchase(products)

  const stepAllProductsInWarehouse = processor.next()
  const stepPriceOfAllProducts = processor.next()
  const stepProductsStoredInDB = processor.next()

  expect(stepProductsStoredInDB.value.event).toEqual('PRODUCTS_STORED_IN_DB')
})
```

W ten sposób przemierzamy przez wszystkie kolejne kroki procesu zawsze starając się testować wyłącznie jeden krok jednocześnie by w razie refaktoryzacji wiedzieć, który krok zawiódł jako pierwszy.

## Generator asynchroniczny

Generatory są synchroniczne, ale można kontrolować ich proces działania poprzez kolejne wywoływanie metody `.next()`. To sprawia, że mogą one również działać współbieżnie. WIększość funkcji w Javascript’cie jest jednak asynchronicznych, jak na przykład wysyłanie maili czy zapis do bazy danych. Generatory domyślnie nie potrafią sobie poradzić z asynchronicznością dlatego od wersji Node’a v10.0.0 dodano [Symbol.asyncIterator](https://nodejs.org/dist/latest-v10.x/docs/api/stream.html#stream_readable_symbol_asynciterator) oraz feature ten jest omawiany [jako propozycja w ES2018 i ES2019](http://exploringjs.com/es2018-es2019/ch_asynchronous-iteration.html#asynchronous-generators).

Przypomnijmy sobie jak wyglądał nasz test dla odrzucenia w drugim kroku. Kiedy przejdziemy na generator asynchroniczny, będzie on wyglądał tak:

```javascript
it('should stop processing further if one of products is not in the warehouse', async () => {
  const products = createProducts(4)
  const processor = processPurchase(products)
  const firstReturn = await processor.next()
  const secondReturn = await processor.next()

  expect(secondReturn.value.event).toEqual('PROCESSING_STOPPED')
  expect(secondReturn.value.cause).toEqual(firstReturn.value.event)

  while ((await processor.next().value) !== undefined) {
    expect(true).toBeFalsy()
  }
})
```

## Rezultat

Skoro wiemy już czym są generatory asynchroniczne, jak tworzyć _pipelines_ z ich użyciem, przedstawiam kod głównej funkcji, która wykonuje cały proces. Dodatkowo zamieszczam adres URL do repozytorium, w którym znajdziecie tę samą funkcję oraz wszystkie testy do niej.

```javascript
module.exports = function(inWarehouse, storeInDB, sendAnEmail) {
  return async function*(products) {
    const notInWarehouse = products.filter(inWarehouse)
    if (notInWarehouse.length > 0) {
      yield {
        event: 'PURCHASED_PRODUCT_IS_NOT_AVAILABLE',
        products,
        not_available: notInWarehouse,
      }
      return {
        event: 'PROCESSING_STOPPED',
        cause: 'PURCHASED_PRODUCT_IS_NOT_AVAILABLE',
      }
    }

    yield { event: 'ALL_PRODUCTS_IN_WAREHOUSE', products }

    const sumPrice = products.reduce((a, b) => a + b.price, 0)
    yield { event: 'SUM_PRICE', price: sumPrice }

    try {
      await storeInDB(products, sumPrice)
      yield { event: 'PRODUCTS_STORED_IN_DB', price: sumPrice, products }
    } catch (e) {
      yield { event: 'CANNOT_STORE_PRODUCTS_INTO_DB', message: e.message }
      return {
        event: 'PROCESSING_STOPPED',
        cause: 'CANNOT_STORE_PRODUCTS_INTO_DB',
      }
    }

    try {
      await sendAnEmail(products)
      yield { event: 'EMAIL_WITH_PRODUCTS_SENT', products }
    } catch (e) {
      yield { event: 'CANNOT_SEND_AN_EMAIL', message: e.message }
      return { event: 'PROCESSING_STOPPED', cause: 'CANNOT_SEND_AN_EMAIL' }
    }

    return { event: 'PROCESSING_STOPPED', cause: 'EMAIL_WITH_PRODUCTS_SENT' }
  }
}
```
