---
path: '/czym-jest-event-loop'
date: '2019-06-22'
title: 'Czym jest Event Loop?'
contentType: 'post'
tags: ['javascript', 'nodejs']
language: 'pl'
cover: './cover.png'
intro: |
  Jeden z najpopularniejszych języków ostatnich lat, język stosowany i na serwerze, i u klienta. Bardzo szybki przyrost bibliotek, frameworków oraz popularności. Ale jak tak naprawdę on działa w środku?
---

Javascript, jako silnik, posiada parę wbudowanych w siebie narzędzi, które pozwalają na operacje na tablicach, konstruowanie zmiennych czy definiowanie funkcji. Nie posiada on jednak żadnego API do obsługi zdarzeń serwerowych, przeglądarek czy systemów mobilnych. W jaki sposób więc Javascript pozwala nam na takie rzeczy?

Przeglądarka oferuje nam DOM API do zarządzania treścią na stronie. Wszystkie elementy jak `Document` lub `Window` są dostarczane przez to API, a sam Javascript _”nie ma pojęcia”_ o ich istnieniu. Identyczna sytuacja jest przy korzystaniu z Node’a, który dostarcza Node.js API do obsługi serwerów. Taka zależność pozwala na _przekazanie pracy do zewnętrznego procesu_, a sam worker javascriptowy robi się wolny od pracy. Mechanika odpowiedzialna za odbieranie pracy od _zewnętrznego procesu_ nazywa się **event loop**, który najprościej można opisać tak:

```javascript
while (queue.waitForMessage()) {
  queue.processNextMessage()
}
```

## Stack, Heap & Queue

Wywoływanie kodu w Javascript oparte jest na trzech fundamentach: **stosie**, **stercie** oraz **kolejce**. Te 3 mechanizmy pozwalają na pracę współbieżną, asynchroniczną oraz na pracę bez blokowania procesu. Spróbujmy sobie to wytłumaczyć z przykładowym kodem:

```javascript
const a = () => console.log('a')
const b = () => console.log('b')
const c = () => console.log('c')

setTimeout(a, 0)
b()
c()
```

Wywołanie takiego kodu sprawi najpierw utworzenie funkcji `main()`, która reprezentuje główny proces aplikacji i zajmuje najniższe miejsce w stacku. Następnie dodane jest wywołanie `setTimeout`, który przekazuje pracę do zewnętrznego procesu, a który natychmiastowo zwróci nam funkcję `a()` do kolejki. Po wywołaniu funkcji `b()` i `c()` oraz gdy zwolni się cały _stack_, _event loop_ przekaże funkcję `a()` do wywołania.

![Event Loop](./event-loop.png)

Pokrótce dokładnie w taki sposób można opisać zasadę wywoływania kodu w Javascript’cie. Wszystkie funkcje trafiają najpierw do stacku, następnie część z nich jest w queue a obiekty i zmienne przetrzymywane są w heapie. Nie ma tutaj żadnych innych magicznych procesów.

## Asynchroniczność i współbieżność

Jeżeli zrozumie się naturę event loopa można stwierdzić, że Javascript nie jest asynchroniczny czy współbieżny. Po części jest to prawda - wszystkie funkcje wywoływane w JS są synchroniczne i blokują główny proces.

To komunikacja z innymi API (Webkit, Node.js) jest asynchroniczna. Funkcja asynchroniczna najczęściej nie jest wywoływana przez silnik Javascript, przykładowo: gdy chcemy odczytać zawartość pliku, dajemy informację do Node’a by dał nam ten plik. On korzystając z procesów systemowych (zależnie od zainstalowanego systemu operacyjnego) otwiera plik, czyta jego zawartość i zwraca do kolejki. Event Loop, jeżeli posiada wolny stack, wciąga tę informację do stosu, a Javascript odbiera już zawartość pliku.

Taka obsługa zewnętrznych zależności wymaga _czekania_ na odpowiedź od Event Loopa. W tym celu kiedyś były wykorzystywane _callbacki_. Taka konstrukcja prowadziła do **callback hell**, co uprawiało czytanie kodu i jego debugging.

```javascript
const fs = require('fs')

fs.readFile('file1.txt', (err, data1) => {
  fs.readFile('file2.txt', (err, data2) => {
    // now I have contents of both files in the same time
  })
})
```

Ta konstrukcja została ulepszona o model Promise’ow, który z czasem został rozszerzony o konstrukcję `async/await`.

```javascript
const util = require('util')
const fs = require('fs')
const readFile = util.promisify(fs.readFile)

const [data1, data2] = await Promise.all([
  readFile('file1.txt'),
  readFile('file2.txt'),
])
```

## Wykorzystanie pętli do optymalizacji wykonywania kodu

Znajomość event loopa można wykorzystać do zewnętrznego wykonywania procesów przy długo trwającym procesie, np. uruchomionym serwerze HTTP. Przykładowy scenariusz może obejmować wysłanie odpowiedzi użytkownikowi kiedy _prawdziwa praca_ wciąż jest wykonywana na serwerze.

```javascript
const createPDF = async (req, res) => {
  setImmediate(async () => {
    const result = await mergePDFFiles(...req.files)
    await saveToDB(result)
  })

  res.status(201).json(/* some JSON body to return */)
}
```

Takie wykorzystanie event loopa posiada jednak wiele wad jak na przykład brak potwierdzenia czy proces się powiódł czy nie lub zniknięcie wszystkich informacji w momencie awarii serwera. Jednakże jeżeli nie potrzebujemy trwałości danych, możemy sobie pozwolić na trochę _oszustwa_.
