---
path: '/gdy-nodejs-nie-jest-asynchroniczny'
date: '2019-09-22'
title: 'Gdy Node.js nie jest asynchroniczny'
contentType: 'post'
tags: ['javascript', 'nodejs']
language: 'pl'
cover: './cover.png'
intro: |
  Wielu programistów ciągnie do pisania aplikacji serwerowych w Node.js z powodu jego asynchroniczności. Czym jest jednak ta asynchroniczność i czy na pewno z niej korzystamy?
---

Node.js posiada wbudowany mechanizm do wykonywania operacji _I/O_ asynchronicznie, nie zatrzymując głównego procesu. Mechanizm ten działa w oparciu o [_event loop_](/czym-jest-event-loop). Jest to naturalny proces działania aplikacji dzięki któremu potrafimy wykonywać różne operacje (żądania do zewnętrznych aplikacji, odczyt pliku lub wykonać akcję _za jakiś czas_) nie czekając na wynik w tym momencie, a wrócić do operacji jak ten wynik dostaniemy.

Asynchroniczność w Node.js działa na _małą_ oraz _dużą_ skalę jednakże nie zawsze _asynchroniczność_ to _asynchroniczność_ opisywana w książkach. Prowadząc dyskusję nt. CQRSa, popularnym wzorcem jest egzekwowanie komend na zasadzie _fire and forget_, w sposób asynchroniczny.

> Commands should follow a fire & forget principle: we should not expect any result from a Command except “OK” or “KO”. A Command is just an ask. The processing can be synchronous or asynchronous, we’re not supposed to get the result asap. The result will come later from the read part.

Jest to fragment wypowiedzi S. Derosiaux’a z jego artykułu [CQRS: Why? And All The Things To Consider](https://www.sderosiaux.com/articles/2019/08/29/cqrs-why-and-all-the-things-to-consider/) gdzie opisuje jak powinno być uruchamianie komend, a precyzując, wpuszczanie komend do _Command Busa_, gdzie na podstawie tych komend są wykonywane akcje. Stéphane tłumaczy jakoby komendy powinny wpuszczane asynchronicznie, a proces, który tego dokonał, nie powinien czekać na odpowiedź. Napiszmy sobie w takim razie prostą czynność, która utworzy komendę i wrzuci ją do

_Command Busa_:

```javascript
function processPayment(req) {
  const command = new ProcessPayment(req.params.id)
  commandBus.execute(command)
    .then(result => res.status(200).json({ result })
    .catch(error => res.status(500).json({ error })
}
```

Akcja `processPayment` przyjmuje za parametr `id` na podstawie którego tworzy komendę `ProcessPayment`. Następnie wrzuca tę komendę do `commandBus` i cała akcja dzieje się… asynchronicznie. `commandBus` zwraca nam instancję `Promise`’a, w którym możemy poczekać na odpowiedź i dopiero wtedy poinformować użytkownika o wyniku.

## To nie jest asynchroniczność

Taka implementacja _fire and forget_ to nie asynchroniczność skoro użytkownik, który wysłał żądanie czeka na wynik operacji. Po wrzuceniu komendy, użytkownik powinien od razu dostać odpowiedź (pozytywną, `200 OK`) a następnie zapytać inny zasób o zmienione dane.

A co w przypadku gdy cały proces możemy upakować w ciąg `Promise`’ów ale dalej zmuszamy użytkownika do czekania aż cały ciąg się wykona, a _side effect_’ów nie ma? Również uważam, że z punktu widzenia użytkownika akcja nie jest wykonywana asynchronicznie. To sprawia, że musimy rozróżniać asynchroniczność na wielu warstwach - nawet w architekturze aplikacji.

## To jest asynchroniczność

Node.js pozwala na synchroniczną akcję wykorzystując asynchroniczne metody. Jest to przydatne w sytuacjach kiedy musimy wykonać wiele operacji _I/O_ w tym samym momencie. Można to bardzo łatwo uzyskać korzystając z wbudowanych metod jak `Promise.all`:

```javascript
const [buyer, item, payment] = await Promise.all([
  someService.fetchBuyer(buyerId),
  someService.fetchItem(itemId),
  someService.fetchPayment(buyerId, itemId),
])
```

W odróżnieniu od synchronicznych języków, tutaj nie musimy wykonywać operacji po operacji tylko wykonać je w tym samym momencie.

## Podsumowanie

Warto zwracać uwagę na to na jakiej warstwie działa nasza _asynchroniczność_. Nie każdą aplikację napisaną w Node.js można nazwać asynchroniczną (czy można?) jak i nie każda aplikacja musi być asynchroniczna. Jednakże pamiętanie o takich detalach może sprawić, że zaczniemy inaczej postrzegać naszą aplikację i znaleźć inne metody jej optymalizacji. Może wprowadzając asynchroniczność?
