---
path: '/komponent-jako-funkcja-czy-jako-klasa'
date: '2019-02-12'
title: 'Komponent jako funkcja, czy jako klasa?'
contentType: 'post'
tags: ['javascript', 'typescript', 'react']
language: 'pl'
cover: './cover.png'
intro: |
  Zespół Facebooka już dawno zadecydował, że tworzenie komponentów jako klasy nie jest najlepszym rozwiązaniem. Włożył bardzo dużo w pracy by usprawnić pracę komponentów funkcjonalnych, a hooki to kolejny krok w tym celu.
---

Nie dawno wyszła kolejna wersja Reacta, numerowana jako _16.8_, która dodała wsparcie dla _hooków_, _”automagicznych”_ funkcji pozwalających na dodawanie nowych funkcjonalności do funkcji. Najmocniejszym dodatkiem jest oczywiście `useState()`, który może nam dodać _stan_ do prostych komponentów.

Przykładowa klasa `Counter` z użyciem stanu jako pełny komponent w React’cie oraz _głupi_ komponent z użyciem _hooków_.

```jsx
import React, { Component } from 'react'

class Counter {
  state = { count: 0 }

  setCount = (count) => this.setState({ count })

  render = () => (
    <div>Ilość: {this.state.count}</div>
    <button onClick={() => this.setCount(this.state.count + 1)}>Zwiększ</button>
  )
}
```

```jsx
import React, { useState } from 'react’

function Counter() {
  const [count, setCount] = useState(0)

  return (
    <div>Ilość: {count}</div>
    <button onClick={() => setCount(count + 1)}>Zwiększ</button>
  )
}
```

Wg. mnie jest to _gamechanger_, który usuwa niepotrzebne, przestarzałe klasy _Component_, _PureComponent_ z powszechnego użycia. Bo tak naprawdę do czego ich potrzebujemy? Do cyklu życia komponentu?

## Cykl życia komponentów

Komponenty w React’cie posiadają swój własny cykl życia opisany metodami:

```typescript
interface ComponentLifecycle<P, S, SS = any>
  extends NewLifecycle<P, S, SS>,
    DeprecatedLifecycle<P, S> {
  componentDidMount?(): void

  shouldComponentUpdate?(
    nextProps: Readonly<P>,
    nextState: Readonly<S>,
    nextContext: any
  ): boolean

  componentWillUnmount?(): void

  componentDidCatch?(error: Error, errorInfo: ErrorInfo): void
}

interface StaticLifecycle<P, S> {
  getDerivedStateFromProps?: GetDerivedStateFromProps<P, S>
  getDerivedStateFromError?: GetDerivedStateFromError<P, S>
}

interface NewLifecycle<P, S, SS> {
  getSnapshotBeforeUpdate?(
    prevProps: Readonly<P>,
    prevState: Readonly<S>
  ): SS | null

  componentDidUpdate?(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot?: SS
  ): void
}

interface DeprecatedLifecycle<P, S> {
  componentWillMount?(): void

  componentWillReceiveProps?(nextProps: Readonly<P>, nextContext: any): void

  componentWillUpdate?(
    nextProps: Readonly<P>,
    nextState: Readonly<S>,
    nextContext: any
  ): void
}
```

Cykl życia pozwala na ingerowanie w stan komponentu pomiędzy różnymi etapami jego życia, np. co powinno się wykonać przed wyświetleniem komponentu lub czy komponent powinien się przebudować jak dostał nowe zmienne.

Prócz `useState()` React 16.8 wprowadził również kolejny hook, którym jest `useEffect()`. Pozwala on na jednoczesne użyczeniem `componentDidMount()` , `componentDidUpdate()` oraz `componentWillUnmount()`.

```jsx
import React, { useEffect, useState } from 'react'
import SubscribeMessages from 'chat'

function NewMessagesIndicator() {
  const [newMessages, setNewMessages] = useState(0)

  function handleIndicatorCounter(num) {
    setNewMessages(num)
  }

  useEffect(() => {
    // componentDidMount()
    // componentDidUpdate()
    SubscribeMessages.subscribe(handleIndicatorCounter)

    return () => {
      // componentWillUnmount()
      SubscribeMessages.unsubscribe()
    }
  })

  return <div>{newMessages}</div>
}
```

Niestety obecna wersja _hooków_ nie posiada nowych rozwiązań dla innych etapów życia komponentu, jednakże jest to wystarczający początek by przepisać nasze `React.Component` na _pure functions_.

## Segregacja kodu w naszych funkcjach

Kolejnym pomocnym argumentem by używać _hooków_ zamiast klas jest segregacja naszego kodu. Metody odpowiedzialne za cykl życia naszych komponentów nie mogły być duplikowane i każda klasa może posiadać tylko jedną taką metodą jednocześnie. Powoduje to wrzucanie różnych _logik biznesowych_ do jednej metody:

```jsx
import React, { Component } from 'react'
import SubscribeMessages from 'chat'

class Homepage extends Component {
  componentDidMount = () => {
    document.tile = 'Strona główna'
    SubscribeMessages.subscribe()
  }

  componentWillUnmount = () => {
    document.write = 'Lorem ipsum'
    SubscribeMessages.unsubscribe()
  }
}
```

Teraz ten sam efekt jesteśmy w stanie uzyskać korzystając z _efektów_ oraz rozdzielając nasz kod zależnie od zastosowania:

```jsx
import React, { Component } from 'react'
import SubscribeMessages from 'chat'

function Homepage() {
  useEffect(() => {
    document.write = 'Strona główna'
    return () => {
      document.write = 'Lorem ipsum'
    }
  })

  useEffect(() => {
    SubscribeMessages.subscribe()
    return () => SubscribeMessages.unsubscribe()
  })
}
```

## Więcej informacji

By lepiej zrozumieć _hooki_ polecam przejrzeć stworzone przez community rozwiązania, np. [zbiór różnych _hooków_ stworzonych przez Gabe’a Raglanda](https://usehooks.com) lub repozytorium [imbhargav5/rooks](https://github.com/imbhargav5/rooks).
