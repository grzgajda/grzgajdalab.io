server {
  listen 80;
  root /usr/share/nginx/html;
  index index.html index.htm;

  location ~* \.(?:manifest|appcache|html?|xml|json)$ {
    expires 1y;
    access_log off;
    add_header Cache-Control "public";
  }

  location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|webp|htc)$ {
    expires 1y;
    access_log off;
    add_header Cache-Control "public";
  }

  location ~* \.(?:css|js)$ {
    expires 1y;
    access_log off;
    add_header Cache-Control "public";
  }

  location / {
    try_files $uri $uri/ /index.html =404;
  }
}