<div align="center">
    <img src="https://gitlab.com/grzgajda/grzgajda.gitlab.io/raw/master/logo.png" alt="devMint" />
</div>

<div align="center">
    <strong>Personal blog developed on Gatsby and React tools. Purpose of this blog is posting about my everyday thoughts about different styles of programming, solving differents problems in another way or just my comments about new framework, tool or library.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-TypeScript-%232b7489.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/grzgajda/grzgajda.gitlab.io.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/badge/coverage-100%25-brightgreen.svg?logo=gitlab&style=for-the-badge" />
</div>

<br />
<br />

Website is built on top of _Gatsby.js_. The source of content are Markdown files localized in _posts_ directory grouped by year. Code highlighting is done by packages _rehype-react_ and _react-highlight_. The whole website is deployed on _GitLab Pages_ using built-in CI and CD proccesses.

## Articles

- [Gdy Node.js nie jest asynchroniczny](https://devmint.pl/gdy-nodejs-nie-jest-asynchroniczny) published at 29/09/2019
- [Czym jest Event Loop?](https://devmint.pl/czym-jest-event-loop) published at 18/08/2019
- [3. 2. 1. Go!](https://devmint.pl/3-2-1-go) published at 17/06/2019
- [Budowanie workflow w oparciu o generatory](https://devmint.pl/budowanie-workflow-w-oparciu-o-generatory) published at 07/05/2019
- [Kompozycja funkcji](https://devmint.pl/kompozycja-funkcji) published at 24/04/2019
- [Witamy tryb nocny - 🌙](https://devmint.pl/witamy-tryb-nocny) published at 31/03/2019
- [HATEOAS - uprawnienia do zasobów](https://devmint.pl/hateoas-uprawnienia-do-zasobow) published 25/03/2019
- [HATEOAS - zróbmy nasze API rozwojowe](https://devmint.pl/hateoas-zrobmy-nasze-api-rozwojowe) published 23/03/2019
- [Generatory, nie listy](https://devmint.pl/generatory-nie-listy) published 03/03/2019
- [Komponent jako funkcja, czy jako klasa?](https://devmint.pl/komponent-jako-funkcja-czy-jako-klasa) published 12/02/2019
- [Generowanie unikalnych ID](https://devmint.pl/generowanie-unikalnych-id) published 01/02/2019
- [styled-is oraz styled-flex - w wersji TypeScript](https://devmint.pl/styled-is-oraz-styled-flex-w-wersji-typescript) published 08/01/2019
- [Konfiguracja Dockera dla Go](https://devmint.pl/konfiguracja-dockera-dla-go) published 30/12/2018
- [Kontener serwisów](https://devmint.pl/kontener-serwisow) published 24/12/2018
- [Segregacja interfejsów](https://devmint.pl/segregacja-interfejsow) published 18/12/2018
- [devMint - a mind of creativity](https://devmint.pl/devmint-a-mind-of-creativity) published 19/11/2018
