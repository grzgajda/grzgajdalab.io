module.exports = function(...families) {
  return {
    options: {
      google: {
        families,
      },
    },
    resolve: 'gatsby-plugin-web-font-loader',
  }
}
