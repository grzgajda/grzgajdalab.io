module.exports = function() {
  return {
    options: {
      host: 'https://devmint.pl',
      sitemap: 'https://devmint.pl/sitemap.xml',
    },
    resolve: 'gatsby-plugin-robots-txt',
  }
}
