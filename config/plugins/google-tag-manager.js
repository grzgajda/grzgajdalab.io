module.exports = function(gtmTag = '') {
  return {
    options: {
      trackingId: gtmTag,
    },
    resolve: `gatsby-plugin-google-analytics`,
  }
}
