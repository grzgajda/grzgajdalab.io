module.exports = function(name, path) {
  return {
    options: {
      name,
      path,
    },
    resolve: 'gatsby-source-filesystem',
  }
}
