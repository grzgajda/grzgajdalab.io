module.exports = function(name, color, icon) {
  return {
    options: {
      /* eslint-disable @typescript-eslint/camelcase */
      background_color: color,
      display: 'fullscreen',
      icon,
      name,
      short_name: name,
      start_url: '/',
      theme_color: color,
    },
    resolve: `gatsby-plugin-manifest`,
  }
}
