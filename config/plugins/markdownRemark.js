module.exports = function() {
  return {
    options: {
      plugins: [
        {
          resolve: 'gatsby-remark-unwrap-images',
        },
        {
          options: {
            // It's important to specify the maxWidth (in pixels) of
            // the content container as this plugin uses this as the
            // base for generating different widths of each image.
            linkImagesToOriginal: false,
            maxWidth: 800,
            showCaptions: false,
            withWebp: true,
          },
          resolve: `gatsby-remark-images`,
        },
      ],
    },
    resolve: 'gatsby-transformer-remark',
  }
}
