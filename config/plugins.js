/* eslint-disable @typescript-eslint/no-var-requires */
const typescript = require('./plugins/typescript')
const reactHelmet = require('./plugins/react-helmet')
const sourceFilesystem = require('./plugins/source-filesystem')
const transformerSharp = require('./plugins/transformer-sharp')
const sharp = require('./plugins/sharp')
const manifest = require('./plugins/manifest')
const offline = require('./plugins/offline')
const markdownRemark = require('./plugins/markdownRemark')
const fontLoader = require('./plugins/fontloader')
const sitemap = require('./plugins/sitemap')
const styledComponents = require('./plugins/styled-components')
const compression = require('./plugins/compression')
const googleTagManager = require('./plugins/google-tag-manager')
const robotsTxt = require('./plugins/robotsTxt')
/* eslint-enable */

module.exports = {
  compression,
  fontLoader,
  googleTagManager,
  manifest,
  markdownRemark,
  offline,
  reactHelmet,
  robotsTxt,
  sharp,
  sitemap,
  sourceFilesystem,
  styledComponents,
  transformerSharp,
  typescript,
}
