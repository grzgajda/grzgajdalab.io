import React from 'react'
import Layout from '../../src/containers/Layout'

export default ({ element }) => <Layout>{element}</Layout>
