// eslint-disable-next-line @typescript-eslint/no-var-requires
const { createFilePath } = require(`gatsby-source-filesystem`)

const createPath = (cnf, node) => (name, value) => cnf({ name, node, value })

module.exports = function({ node, getNode, actions }) {
  if (node !== undefined && node.internal.type !== 'MarkdownRemark') {
    return { actions, getNode, node }
  }

  const slug = createFilePath({ basePath: `posts`, getNode, node })
  const createPathForNode = createPath(actions.createNodeField, node)
  const {
    path,
    contentType,
    tags,
    language,
    cover,
    intro,
    shortTitle,
    status,
    title,
  } = node.frontmatter

  // for posts
  createPathForNode('slug', path || slug)
  createPathForNode('contentType', contentType || 'basic')
  createPathForNode('tags', tags || [])
  createPathForNode('language', language || 'pl')
  createPathForNode('cover', cover || null)
  createPathForNode('intro', intro || '')
  createPathForNode('status', status || 'published')

  // for tags
  createPathForNode('shortTitle', shortTitle || title.toLowerCase())

  return { actions, getNode, node }
}
