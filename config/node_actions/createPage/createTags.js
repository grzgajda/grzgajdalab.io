// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require(`path`)

module.exports = ({ node }) => {
  const { seoDescription, title, shortTitle } = node.frontmatter
  const context = {
    seoDescription,
    slug: node.fields.slug,
    slugName: shortTitle || title.toLowerCase(),
    tagName: title,
  }

  return {
    component: path.resolve('./src/templates/TagBrowser/index.tsx'),
    context,
    path: context.slug,
  }
}
