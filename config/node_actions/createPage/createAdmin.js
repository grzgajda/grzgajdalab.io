// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require(`path`)

const createPage = (template, pagePath) => ({
  component: path.resolve(template),
  context: {},
  path: pagePath,
})

module.exports = () => {
  if (process.env.NODE_ENV !== 'development') {
    return []
  }

  return [createPage('./src/templates/AdminPosts/index.tsx', '__admin/posts')]
}
