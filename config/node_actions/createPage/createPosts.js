// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require(`path`)

module.exports = ({ node }) => {
  const { frontmatter, excerpt } = node
  const { tags, title } = frontmatter
  const context = {
    excerpt,
    slug: node.fields.slug,
    tags,
    title,
  }

  return {
    component: path.resolve('./src/templates/BlogPost/index.tsx'),
    context,
    path: context.slug,
  }
}
