/* eslint-disable @typescript-eslint/no-var-requires */
const createTags = require('./createPage/createTags')
const createPosts = require('./createPage/createPosts')
const createAdmin = require('./createPage/createAdmin')
/* eslint-enable */

async function markdownQuery(graphql) {
  return await graphql(`
    query OnlyPostsSlug {
      posts: allMarkdownRemark(
        filter: { fields: { contentType: { eq: "post" } } }
      ) {
        edges {
          node {
            frontmatter {
              title
              tags
            }
            excerpt(pruneLength: 140)
            fields {
              slug
            }
          }
        }
      }
      tags: allMarkdownRemark(
        filter: { fields: { contentType: { eq: "tag" } } }
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              title
              shortTitle
              seoDescription
            }
          }
        }
      }
    }
  `)
}

module.exports = async function(graphql) {
  const results = await markdownQuery(graphql)
  if (results.errors) {
    throw new Error(results.errors)
  }

  const { tags, posts } = results.data
  const filteredTags = tags.edges
  const filteredPosts = posts.edges

  return [
    ...filteredTags.map(createTags),
    ...filteredPosts.map(createPosts),
    ...createAdmin(),
  ]
}
