declare module 'rehype-react' {
  // eslint-disable-next-line @typescript-eslint/interface-name-prefix
  interface Options {
    components?: object
    createElement?: function
    prefix?: string
  }

  // eslint-disable-next-line @typescript-eslint/interface-name-prefix
  interface Node {
    type: string
    tagName: string
    properties: object
    children?: Node[]
  }

  // eslint-disable-next-line @typescript-eslint/interface-name-prefix
  interface Components {
    [key: string]: React.ReactElement
  }

  /* eslint-disable @typescript-eslint/explicit-member-accessibility */
  export default class {
    constructor(Options)
    Compiler(Node): React.ReactElement
  }
}
