/* eslint-disable @typescript-eslint/no-var-requires */
const markdownContentType = require('./config/node_actions/markdownContentType')
const markdownCreatePage = require('./config/node_actions/markdownCreatePage')
/* eslint-enable */

exports.onCreateNode = ({ node, getNode, actions }) => {
  markdownContentType({ actions, getNode, node })
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const pages = await markdownCreatePage(graphql)

  pages.forEach(page => {
    createPage(page)
  })
}
