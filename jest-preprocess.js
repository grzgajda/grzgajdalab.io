const babelOptions = {
  plugins: [
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-proposal-class-properties',
  ],
  presets: ['@babel/react', '@babel/env'],
}

module.exports = require('babel-jest').createTransformer(babelOptions)
